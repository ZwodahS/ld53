package ui;

import zf.ui.ScaleGridFactory;

class TooltipWindow extends UIElement {
	public var bgFactory: ScaleGridFactory;

	public var title: String;
	public var titleFont: String = "display.1";
	public var tooltip: String;
	public var tooltipFont: String = "body.0";

	public var minWidth: Int = 0;
	public var maxWidth: Int = 550;
	public var minHeight: Null<Int> = 50;

	public var spacing(default, set): Float = 0;

	public var text: HtmlText;

	public function set_spacing(f: Float): Float {
		this.spacing = f;
		if (this.text != null) this.text.lineSpacing = f;
		return f;
	}

	public function new(tooltip: String = null, title: String = null) {
		super();
		this.bgFactory = Assets.gridFactory["box1"];
		this.title = title;
		this.tooltip = tooltip;
		this.showDelay = .25;
		this.useShowDelay = true;
	}

	dynamic public function getTooltip(): String {
		return this.tooltip;
	}

	dynamic public function getTitle(): String {
		return this.title;
	}

	override public function onShow() {
		this.removeChildren();
		var tooltipString = getTooltip();
		if (tooltipString == null) {
			this.visible = false;
			return;
		}

		final flow = G.ui.makeObjectFromXMLString('<layout-vflow spacing="10"></layout-vflow>');
		final title = this.getTitle();
		if (title != null && title != "") {
			final text = G.ui.makeObjectFromXMLString('
				<text fontName="${this.titleFont}" textColor="black">${title}</text>
			');
			flow.addChild(text);
		}

		final text = G.ui.makeObjectFromXMLString('
			<text fontName="body.1" textColor="black" maxWidth="${this.maxWidth - 30}">${tooltipString}</text>
		');
		flow.addChild(text);

		this.text = cast text;
		this.text.lineSpacing = this.spacing;

		final obj = G.ui.makeObjectFromStruct({
			type: "window",
			conf: {
				item: {
					type: "object",
					conf: {
						object: flow,
					}
				},
				bgFactory: this.bgFactory,
				minWidth: this.minWidth,
				maxWidth: this.maxWidth,
				paddings: [4, 2, 4, 2]
			}
		});
		this.addChild(obj);
		this.visible = false;
	}
}
