package ui;

class GearDisplay extends UIElement {
	public var display: h2d.Object;

	var system: RenderSystem;

	public var arrow: h2d.Object;

	public var percentage(default, set): Float;

	var targetRotation: Float;

	public function set_percentage(v: Float): Float {
		this.percentage = Math.clampF(v, 0, 1);
		this.targetRotation = Math.PI + (Math.PI * this.percentage);
		return this.percentage;
	}

	public var gear(default, set): Int;

	public function set_gear(v: Int): Int {
		this.gear = v;
		this.percentage = v / 4;
		return this.gear;
	}

	public function new(system: RenderSystem) {
		super();
		this.system = system;

		this.addChild(this.display = new h2d.Object());

		final arrow = Assets.fromColor(0xfffffbe5, 22, 2);
		arrow.x = -1;
		arrow.y = -1;
		this.arrow = new h2d.Object();
		this.arrow.addChild(arrow);
		this.arrow.setX(32).setY(32);
		this.display.addChild(this.arrow);

		this.percentage = 0;
		this.arrow.rotation = Math.PI;
		this.gear = 0;

		this.display.addChild(this.interactive = new Interactive(65, 37));
		final window = new TooltipWindow("");
		window.getTooltip = () -> {
			if (this.system.worldState == null) return null;
			final speedString = [];
			for (gear => speed in this.system.worldState.player.state.speed) {
				speedString.push('Gear ${gear + 1}: ${speed} min per movement');
			}
			return speedString.join("\n");
		};
		this.tooltipWindow = window;
	}

	public function update(dt: Float) {
		if (this.targetRotation != this.arrow.rotation) {
			if (this.targetRotation > this.arrow.rotation) {
				this.arrow.rotation += Math.clampF(5 * dt, 0, (this.targetRotation - this.arrow.rotation));
			} else {
				this.arrow.rotation -= Math.clampF(5 * dt, 0, (this.arrow.rotation - this.targetRotation));
			}
		}
	}

	public function init(world: World) {
		final dispatcher = world.dispatcher;

		// @:listen LevelDisplay MOnWorldStateSet 50
		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			onLoad();
		}, 50);

		dispatcher.listen(MOnGearChange.MessageType, (message: zf.Message) -> {
			final m: MOnGearChange = cast message;
			handleMOnGearChange(m);
		}, 100);
	}

	function onLoad() {
		// we will add all the entity into the world
		final player = this.system.world.worldState.player;
		this.gear = player.state.gear;
	}

	function handleMOnGearChange(m: MOnGearChange) {
		this.gear = this.system.world.worldState.player.state.gear;
	}
}
