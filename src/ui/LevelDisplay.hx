package ui;

class LevelDisplay extends UIElement {
	public var display: h2d.Layers;

	public var system: RenderSystem;

	public var world(get, never): World;

	public function get_world(): World {
		return this.system.world;
	}

	public function new(system: RenderSystem) {
		super();
		this.system = system;

		this.addChild(this.display = new h2d.Layers());
	}

	public function init(world: World) {
		final dispatcher = world.dispatcher;

		// @:listen LevelDisplay MOnWorldStateSet 50
		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			onLoad();
		}, 50);

		dispatcher.listen(MOnPlayerMove.MessageType, (message: zf.Message) -> {
			final m: MOnPlayerMove = cast message;
			handleMOnPlayerMove(m);
		}, 10);
	}

	function onLoad() {
		this.display.removeChildren();
		// we will add all the entity into the world
		for (entity in this.world.worldState.entities) {
			renderEntity(entity);
		}
	}

	function renderEntity(entity: Entity) {
		switch (entity.kind) {
			case Road:
				this.display.add(entity.render.map, entity.position.y);
				entity.render.map.x = C.GridSize.x * entity.position.x;
				entity.render.map.y = (C.GridSize.y * entity.position.y);
			case Building, Player, Fixture:
				this.display.add(entity.render.map, 1000 + entity.position.y);
				entity.render.map.x = C.GridSize.x * entity.position.x;
				entity.render.map.y = (C.GridSize.y * entity.position.y);
			default:
		}
	}

	function handleMOnPlayerMove(m: MOnPlayerMove) {
		final entity = this.world.worldState.player;
		// update the rendering layer
		this.display.add(entity.render.map, 1000 + m.destination.y);
		// animate
		this.world.state.set(GameState.PlayerMoving);
		this.world.updater.run(new MoveToLocationByDuration(entity.render.map.wo(),
			[C.GridSize.x * entity.position.x, (C.GridSize.y * entity.position.y)], 0.25).whenDone(() -> {
			if (this.world.state.is(GameState.PlayerMoving) == true) this.world.state.set(GameState.Idle);
			this.world.dispatcher.dispatch(new MOnPlayerMoveFinish());
		}));
	}
}
