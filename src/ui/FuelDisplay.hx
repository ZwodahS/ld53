package ui;

class FuelDisplay extends UIElement {
	public var display: h2d.Object;

	var fuelDisplay: BitmapDisplay;

	var system: RenderSystem;

	public var arrow: h2d.Object;

	public var percentage(default, set): Float;

	var targetRotation: Float;

	public function set_percentage(v: Float): Float {
		this.percentage = Math.clampF(v, 0, 1);
		this.targetRotation = Math.PI + (Math.PI * this.percentage);
		return this.percentage;
	}

	public var maxFuel(default, set): Int;

	public function set_maxFuel(v: Int): Int {
		this.maxFuel = v;
		this.percentage = this.fuel / this.maxFuel;
		return this.maxFuel;
	}

	public var fuel(default, set): Int;

	public function set_fuel(v: Int): Int {
		this.fuel = v;
		this.percentage = this.fuel / this.maxFuel;
		this.fuelDisplay.value = '${this.fuel}';
		return this.fuel;
	}

	public function new(system: RenderSystem) {
		super();
		this.system = system;

		this.addChild(this.display = new h2d.Object());

		// since the fuel is part of the background, we don't have to display it.
		this.display.addChild(this.fuelDisplay = new BitmapDisplay("99"));
		this.fuelDisplay.alignment = AnchorRight;
		this.fuelDisplay.x = 52;
		this.fuelDisplay.y = 24;

		final arrow = Assets.fromColor(0xfffffbe5, 22, 2);
		arrow.x = -1;
		arrow.y = -1;
		this.arrow = new h2d.Object();
		this.arrow.addChild(arrow);
		this.arrow.setX(32).setY(32);
		this.display.addChild(this.arrow);

		this.percentage = 0;
		this.arrow.rotation = Math.PI;
		this.maxFuel = 1;
		this.fuel = 0;

		this.display.addChild(this.interactive = new Interactive(65, 37));
		final window = new TooltipWindow("");
		window.getTooltip = () -> {
			return 'Fuel: ${this.system.worldState.player.state.fuel} / ${this.system.worldState.player.state.maxFuel}';
		};
		this.tooltipWindow = window;
	}

	public function update(dt: Float) {
		if (this.targetRotation != this.arrow.rotation) {
			if (this.targetRotation > this.arrow.rotation) {
				this.arrow.rotation += Math.clampF(5 * dt, 0, (this.targetRotation - this.arrow.rotation));
			} else {
				this.arrow.rotation -= Math.clampF(5 * dt, 0, (this.arrow.rotation - this.targetRotation));
			}
		}
	}

	public function init(world: World) {
		final dispatcher = world.dispatcher;

		// @:listen LevelDisplay MOnWorldStateSet 50
		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			onLoad();
		}, 50);

		dispatcher.listen(MOnFuelChange.MessageType, (message: zf.Message) -> {
			final player = world.worldState.player;
			this.fuel = player.state.fuel;
		}, 0);

		dispatcher.listen(MOnMaxFuelChange.MessageType, (message: zf.Message) -> {
			final player = world.worldState.player;
			this.maxFuel = player.state.maxFuel;
		}, 0);
	}

	function onLoad() {
		// we will add all the entity into the world
		final player = this.system.world.worldState.player;
		this.fuel = player.state.fuel;
		this.maxFuel = player.state.maxFuel;
	}
}
