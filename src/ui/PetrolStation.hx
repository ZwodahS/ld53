package ui;

class PetrolMenuButton extends UIElement {
	public static final Size: Point2i = [25, 9];

	public var display: h2d.Object;

	public var frames: h2d.Anim;

	public function new(text: String) {
		super();

		this.addChild(this.display = new h2d.Object());
		this.display.addChild(this.frames = Assets.res.getAnim('petrolstation:menu'));
		this.frames.pause = true;

		final bm = new BitmapDisplay(text);
		bm.x = 12.5;
		bm.y = 2;
		bm.alignment = AlignCenter;
		this.display.addChild(bm);

		this.display.addChild(this.interactive = new Interactive(Size.x, Size.y));
	}

	override function updateRendering() {
		this.frames.currentFrame = 0;

		if (this.disabled == true) {
			this.frames.currentFrame = 0;
		} else if (this.isOver == true) {
			this.frames.currentFrame = 1;
		} else {
			this.frames.currentFrame = 0;
		}
	}
}

class PetrolStation extends UIElement {
	public var display: h2d.Object;

	public var system: RenderSystem;

	public function new(system: RenderSystem) {
		super();
		this.system = system;

		this.addChild(Assets.res.getBitmap("screen:petrolstation"));
		this.addChild(this.display = new h2d.Object());

		this.visible = false;

		final flow = G.ui.make('<layout-vflow spacing="1" />');

		final button = new PetrolMenuButton("100F");
		button.addOnClickListener("PetrolStation", (e) -> {
			buy(100, 85);
		});
		flow.addChild(button);

		final button = new PetrolMenuButton("50F");
		button.addOnClickListener("PetrolStation", (e) -> {
			buy(50, 45);
		});
		flow.addChild(button);

		final button = new PetrolMenuButton("20F");
		button.addOnClickListener("PetrolStation", (e) -> {
			buy(20, 19);
		});
		flow.addChild(button);

		final button = new PetrolMenuButton("1F");
		button.addOnClickListener("PetrolStation", (e) -> {
			buy(1, 1);
		});
		flow.addChild(button);

		final button = new PetrolMenuButton("OKAY");
		button.addOnClickListener("PetrolStation", (e) -> {
			this.close();
		});
		flow.addChild(button);

		flow.x = 87;
		flow.y = 37;
		this.display.addChild(flow);
	}

	public function init(world: World) {
		final dispatcher = world.dispatcher;

		dispatcher.listen(MDoOpenPetrol.MessageType, (message: zf.Message) -> {
			final m: MDoOpenPetrol = cast message;
		}, 1);
	}

	function close() {
		this.system.closePetrol();
	}

	function buy(fuel: Int, cost: Int) {
		if (this.system.world.worldState.money < cost) return;
		final player = this.system.world.worldState.player;
		if (player.state.fuel == player.state.maxFuel) return;
		this.system.world.worldState.money -= cost;
		this.system.dispatcher.dispatch(new MOnGoldChange());
		player.state.fuel = Math.clampI(player.state.fuel + fuel, 0, player.state.maxFuel);
		this.system.dispatcher.dispatch(new MOnFuelChange());
	}
}
