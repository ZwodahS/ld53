package ui;

class BitmapDisplay extends UIElement {
	public var display: h2d.Object;

	public var value(default, set): String;

	public var color(default, set): Color = 0xffffffff;

	public var alignment(default, set): zf.h2d.ObjectExtensions.SetMode = AlignCenter;

	public function set_alignment(v: zf.h2d.ObjectExtensions.SetMode): zf.h2d.ObjectExtensions.SetMode {
		this.alignment = v;
		this.display.setX(0, this.alignment);
		return this.alignment;
	}

	public function set_value(v: String): String {
		this.value = v;
		this.display.removeChildren();
		Assets.displayStr(this.value, this.display, this.color);
		this.display.setX(0, this.alignment);
		return this.value;
	}

	public function set_color(c: Color): Color {
		this.color = c;
		this.display.removeChildren();
		Assets.displayStr(this.value, this.display, this.color);
		this.display.setX(0, this.alignment);
		return this.color;
	}

	public function new(value: String = "") {
		super();
		this.display = new h2d.Object();
		this.addChild(this.display);
		this.value = value;
	}
}
