package ui;

class QuestButton extends UIElement {
	public var display: h2d.Layers;

	public var color(default, set): String;

	public var hideOnDisable: Bool = false;

	public function set_color(v: String): String {
		this.color = v;
		if (this.buttonFrames != null) this.buttonFrames.remove();
		this.display.add(this.buttonFrames = Assets.res.getAnim('quest:bubble:${this.color}'), 0);
		this.buttonFrames.pause = true;
		return this.color;
	}

	public var icon(default, set): String;

	public function set_icon(v: String): String {
		this.icon = v;
		if (this.iconObject != null) this.iconObject.remove();
		this.display.add(this.iconObject = Assets.res.getBitmap('quest:${this.icon}'), 1);
		return this.icon;
	}

	var buttonFrames: h2d.Anim;
	var iconObject: h2d.Object;

	public var building: Building;

	public function new(color: String, icon: String) {
		super();

		this.addChild(this.display = new h2d.Layers());
		this.color = color;
		this.icon = icon;
		this.display.add(this.interactive = new Interactive(16, 13), 2);
	}

	override function updateRendering() {
		if (this.disabled == true) {
			if (this.hideOnDisable == true) {
				this.visible = false;
			} else {
				this.buttonFrames.currentFrame = 2;
			}
		} else if (this.isOver == true) {
			this.visible = true;
			this.buttonFrames.currentFrame = 1;
		} else {
			this.visible = true;
			this.buttonFrames.currentFrame = 0;
		}
	}
}
