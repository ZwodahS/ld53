package ui;

class HelpWindow extends UIElement {
	public var display: h2d.Object;

	public var bg: h2d.Object;

	public static final Xml = '
		<layout-vflow spacing="10">
			<text textColor="black" fontName="display.3">Tutorial</text>
			<layout-vflow spacing="5">
				<text textColor="black" fontName="display.2">Control</text>
				<text textColor="black" fontName="display.1">
					- W,S,A,D or Arrow key to move the Car.
					- Click on the bubble <img src="texticon:bubble" /> to interact with the building.
					- Hover over the bubble to see what they do.
				</text>
			</layout-vflow>
			<layout-vflow spacing="5">
				<text textColor="black" fontName="display.2">Instructions</text>
				<text textColor="black" fontName="display.1">
					- You must deliver a minimum amount of Parcels every day (increase by 2 per day up to 20).
					- You must report back to the warehouse by the end of the day (11pm), after finishing all delivery.
					- If you move in the same direction, the gear of the car will increase.
					- When you perform a 90 degree turn, the gear is reduced by 3.
					- When you perform a 180 degree turn, the gear is reset to 1.
					- The faster you are moving, the less time it take to move. (Hover over the Gear Display to see the speed).
					- Each move cost 1 Fuel.
					- Refuel at petrol station.
					- Performing any action (refuel, deliver etc) will reset gear to 1.
					- Upgrade your vehicle at the mechanic.
				</text>
			</layout-vflow>
		</layout-vflow>
	';

	public function new() {
		super();

		this.addChild(this.bg = Assets.fromColor(Colors.Blacks[0], Globals.game.gameWidth, Globals.game.gameHeight));
		this.bg.alpha = 0.5;

		this.addChild(this.display = new h2d.Object());
		this.display.addChild(Assets.gridFactory["box1"].make([400, 220]));
		this.display.setX(Globals.game.gameWidth, AlignCenter).setY(Globals.game.gameHeight, AlignCenter);

		final object = G.ui.make(Xml);
		object.x = 4;
		object.y = 4;
		this.display.addChild(object);

		this.addChild(this.interactive = new Interactive(Globals.game.gameWidth, Globals.game.gameHeight));
		this.addOnClickListener("HelpWindow", (e) -> {
			close();
		});
	}

	public function close() {
		this.remove();
	}
}
