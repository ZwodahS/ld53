package ui;

class UpgradeState extends UIElement {
	public var display: h2d.Object;

	public var frames: h2d.Anim;
	public var isUpgraded(default, set): Bool = false;

	public function set_isUpgraded(v: Bool): Bool {
		this.isUpgraded = v;
		this.frames.currentFrame = this.isUpgraded ? 1 : 0;
		return this.isUpgraded;
	}

	public function new(clipboard: Clipboard) {
		super();

		this.addChild(this.display = new h2d.Object());
		this.display.addChild(this.frames = Assets.res.getAnim('mechanic:upgradeState'));
		this.frames.pause = true;
	}
}

class ClipboardCloseButton extends UIElement {
	public static final Size: Point2i = [19, 9];

	public var display: h2d.Object;

	public var frames: h2d.Anim;

	public function new() {
		super();

		this.addChild(this.display = new h2d.Object());
		this.display.addChild(this.frames = Assets.res.getAnim('mechanic:exit'));
		this.frames.pause = true;

		this.display.addChild(this.interactive = new Interactive(Size.x, Size.y));
	}

	override function updateRendering() {
		this.frames.currentFrame = 0;

		if (this.disabled == true) {
			this.frames.currentFrame = 0;
		} else if (this.isOver == true) {
			this.frames.currentFrame = 1;
		} else {
			this.frames.currentFrame = 0;
		}
	}
}

class ClipboardUpgradeButton extends UIElement {
	public static final Size: Point2i = [50, 9];

	public var display: h2d.Object;

	public var frames: h2d.Anim;

	var bm: BitmapDisplay;

	public var amount(default, set): Int;

	public function set_amount(v: Int): Int {
		this.amount = v;
		bm.value = '${this.amount}';
		return this.amount;
	}

	public function new(amount: Int) {
		super();

		this.addChild(this.display = new h2d.Object());
		this.display.addChild(this.frames = Assets.res.getAnim('mechanic:upgrade'));
		this.frames.pause = true;

		this.bm = new BitmapDisplay('${amount}');
		bm.x = 48;
		bm.y = 2;
		bm.alignment = AnchorRight;
		this.display.addChild(bm);

		this.display.addChild(this.interactive = new Interactive(Size.x, Size.y));

		this.amount = amount;
	}

	override function updateRendering() {
		this.frames.currentFrame = 0;

		if (this.disabled == true) {
			this.frames.currentFrame = 0;
			this.bm.color = 0xfffffbe5;
		} else if (this.isOver == true) {
			this.frames.currentFrame = 1;
			this.bm.color = 0xff44f720;
		} else {
			this.frames.currentFrame = 0;
			this.bm.color = 0xfffffbe5;
		}
	}
}

class Clipboard extends UIElement {
	public var display: h2d.Object;

	public var system: RenderSystem;

	// public var foodUB: ClipboardUpgradeButton;
	// public var seatUB: ClipboardUpgradeButton;
	public var fuelUB: ClipboardUpgradeButton;
	public var gear1UB: ClipboardUpgradeButton;
	public var gear2UB: ClipboardUpgradeButton;
	public var gear3UB: ClipboardUpgradeButton;
	public var gear4UB: ClipboardUpgradeButton;
	public var gear5UB: ClipboardUpgradeButton;

	// public var foodUpgradeState: Array<UpgradeState>;
	// public var seatUpgradeState: Array<UpgradeState>;
	public var fuelUpgradeState: Array<UpgradeState>;
	public var gear1UpgradeState: Array<UpgradeState>;
	public var gear2UpgradeState: Array<UpgradeState>;
	public var gear3UpgradeState: Array<UpgradeState>;
	public var gear4UpgradeState: Array<UpgradeState>;
	public var gear5UpgradeState: Array<UpgradeState>;

	// public var foodCosts: Array<Int> = [25];
	// public var seatCosts: Array<Int> = [50];
	public var fuelCosts: Array<Int> = [20, 30, 40, 50];
	public var gear1Costs: Array<Int> = [10, 20];
	public var gear2Costs: Array<Int> = [10, 20];
	public var gear3Costs: Array<Int> = [10, 20];
	public var gear4Costs: Array<Int> = [10, 20];
	public var gear5Costs: Array<Int> = [10, 20];

	public var closeButton: ClipboardCloseButton;

	public function new(system: RenderSystem) {
		super();

		this.system = system;

		this.addChild(Assets.res.getBitmap("screen:mechanicboard"));
		this.addChild(this.display = new h2d.Object());

		this.visible = false;

		/**
			this.display.addChild(this.foodUB = new ClipboardUpgradeButton(foodCosts[0]));
			this.foodUB.tooltipWindow = new TooltipWindow(S.get("upgrades.food.tooltip"));
			this.foodUB.tooltipShowConf = {preferredDirection: [Right, Up, Left, Down]};
			this.foodUB.tooltipHelper = system.tooltipHelper;
			this.foodUB.getTooltipBounds = () -> {
				return this.foodUB.getBounds(system.drawLayers);
			}
			this.foodUB.addOnClickListener("Clipboard", (e) -> {
				final worldState = this.system.world.worldState;
				if (worldState == null) return;
				if (worldState.hasFoodDelivery == true) return;
				if (worldState.money < this.foodCosts[0]) return;
				worldState.money -= this.foodCosts[0];
				this.system.dispatcher.dispatch(new MOnGoldChange());
				worldState.hasFoodDelivery = true;
				this.foodUpgradeState[0].isUpgraded = true;
				this.foodUB.visible = false;
			});
			this.foodUB.setX(83).setY(30);

			this.display.addChild(this.seatUB = new ClipboardUpgradeButton(seatCosts[0]));
			this.seatUB.tooltipWindow = new TooltipWindow(S.get("upgrades.seat.tooltip"));
			this.seatUB.tooltipShowConf = {preferredDirection: [Right, Up, Left, Down]};
			this.seatUB.tooltipHelper = system.tooltipHelper;
			this.seatUB.getTooltipBounds = () -> {
				return this.seatUB.getBounds(system.drawLayers);
			}
			this.seatUB.addOnClickListener("Clipboard", (e) -> {
				final worldState = this.system.world.worldState;
				if (worldState == null) return;
				if (worldState.hasPeopleDelivery == true) return;
				if (worldState.money < this.seatCosts[0]) return;
				worldState.money -= this.seatCosts[0];
				this.system.dispatcher.dispatch(new MOnGoldChange());
				worldState.hasPeopleDelivery = true;
				this.seatUpgradeState[0].isUpgraded = true;
				this.seatUB.visible = false;
			});
			this.seatUB.setX(83).setY(45);
		**/

		this.display.addChild(this.fuelUB = new ClipboardUpgradeButton(fuelCosts[0]));
		this.fuelUB.tooltipWindow = new TooltipWindow(S.get("upgrades.fuel.tooltip"));
		this.fuelUB.tooltipShowConf = {preferredDirection: [Right, Up, Left, Down]};
		this.fuelUB.tooltipHelper = system.tooltipHelper;
		this.fuelUB.getTooltipBounds = () -> {
			return this.fuelUB.getBounds(system.drawLayers);
		}
		this.fuelUB.addOnClickListener("Clipboard", (e) -> {
			final worldState = this.system.world.worldState;
			if (worldState == null) return;
			if (worldState.fuelCapacity >= this.fuelCosts.length) return;
			if (worldState.money < this.fuelCosts[worldState.fuelCapacity]) return;
			worldState.money -= this.fuelCosts[worldState.fuelCapacity];
			this.system.dispatcher.dispatch(new MOnGoldChange());
			this.fuelUpgradeState[worldState.fuelCapacity].isUpgraded = true;
			worldState.fuelCapacity += 1;
			worldState.player.state.maxFuel += 20;
			this.system.world.dispatcher.dispatch(new MOnMaxFuelChange());
			if (worldState.fuelCapacity == this.fuelCosts.length) this.fuelUB.visible = false;
			if (worldState.fuelCapacity < fuelCosts.length) fuelUB.amount = fuelCosts[worldState.fuelCapacity];
		});
		this.fuelUB.setX(83).setY(59);

		this.display.addChild(this.gear1UB = new ClipboardUpgradeButton(gear1Costs[0]));
		this.gear1UB.tooltipWindow = new TooltipWindow(S.get("upgrades.gear.tooltip"));
		this.gear1UB.tooltipShowConf = {preferredDirection: [Right, Up, Left, Down]};
		this.gear1UB.tooltipHelper = system.tooltipHelper;
		this.gear1UB.getTooltipBounds = () -> {
			return this.gear1UB.getBounds(system.drawLayers);
		}
		this.gear1UB.addOnClickListener("Clipboard", (e) -> {
			final worldState = this.system.world.worldState;
			if (worldState == null) return;
			if (worldState.gear1 >= this.gear1Costs.length) return;
			if (worldState.money < this.gear1Costs[worldState.gear1]) return;
			worldState.money -= this.fuelCosts[worldState.gear1];
			this.system.dispatcher.dispatch(new MOnGoldChange());
			this.gear1UpgradeState[worldState.gear1].isUpgraded = true;
			worldState.gear1 += 1;
			worldState.player.state.speed[0] -= 1;
			if (worldState.gear1 == this.gear1Costs.length) this.gear1UB.visible = false;
			if (worldState.gear1 < gear1Costs.length) gear1UB.amount = gear1Costs[worldState.gear1];
		});
		this.gear1UB.setX(83).setY(74);

		this.display.addChild(this.gear2UB = new ClipboardUpgradeButton(gear2Costs[0]));
		this.gear2UB.tooltipWindow = new TooltipWindow(S.get("upgrades.gear.tooltip"));
		this.gear2UB.tooltipShowConf = {preferredDirection: [Right, Up, Left, Down]};
		this.gear2UB.tooltipHelper = system.tooltipHelper;
		this.gear2UB.getTooltipBounds = () -> {
			return this.gear2UB.getBounds(system.drawLayers);
		}
		this.gear2UB.addOnClickListener("Clipboard", (e) -> {
			final worldState = this.system.world.worldState;
			if (worldState == null) return;
			if (worldState.gear2 >= this.gear2Costs.length) return;
			if (worldState.money < this.gear2Costs[worldState.gear2]) return;
			worldState.money -= this.gear2Costs[worldState.gear2];
			this.system.dispatcher.dispatch(new MOnGoldChange());
			this.gear2UpgradeState[worldState.gear2].isUpgraded = true;
			worldState.gear2 += 1;
			worldState.player.state.speed[1] -= 1;
			if (worldState.gear2 == this.gear2Costs.length) this.gear2UB.visible = false;
			if (worldState.gear2 < gear2Costs.length) gear2UB.amount = gear2Costs[worldState.gear2];
		});
		this.gear2UB.setX(83).setY(85);

		this.display.addChild(this.gear3UB = new ClipboardUpgradeButton(gear3Costs[0]));
		this.gear3UB.tooltipWindow = new TooltipWindow(S.get("upgrades.gear.tooltip"));
		this.gear3UB.tooltipShowConf = {preferredDirection: [Right, Up, Left, Down]};
		this.gear3UB.tooltipHelper = system.tooltipHelper;
		this.gear3UB.getTooltipBounds = () -> {
			return this.gear3UB.getBounds(system.drawLayers);
		}
		this.gear3UB.addOnClickListener("Clipboard", (e) -> {
			final worldState = this.system.world.worldState;
			if (worldState == null) return;
			if (worldState.gear3 >= this.gear3Costs.length) return;
			if (worldState.money < this.gear3Costs[worldState.gear3]) return;
			worldState.money -= this.gear3Costs[worldState.gear3];
			this.system.dispatcher.dispatch(new MOnGoldChange());
			this.gear3UpgradeState[worldState.gear3].isUpgraded = true;
			worldState.gear3 += 1;
			worldState.player.state.speed[2] -= 1;
			if (worldState.gear3 == this.gear3Costs.length) this.gear3UB.visible = false;
			if (worldState.gear3 < gear3Costs.length) gear3UB.amount = gear3Costs[worldState.gear3];
		});
		this.gear3UB.setX(83).setY(96);

		this.display.addChild(this.gear4UB = new ClipboardUpgradeButton(gear4Costs[0]));
		this.gear4UB.tooltipWindow = new TooltipWindow(S.get("upgrades.gear.tooltip"));
		this.gear4UB.tooltipShowConf = {preferredDirection: [Right, Up, Left, Down]};
		this.gear4UB.tooltipHelper = system.tooltipHelper;
		this.gear4UB.getTooltipBounds = () -> {
			return this.gear4UB.getBounds(system.drawLayers);
		}
		this.gear4UB.addOnClickListener("Clipboard", (e) -> {
			final worldState = this.system.world.worldState;
			if (worldState == null) return;
			if (worldState.gear4 >= this.gear4Costs.length) return;
			if (worldState.money < this.gear4Costs[worldState.gear4]) return;
			worldState.money -= this.gear4Costs[worldState.gear4];
			this.system.dispatcher.dispatch(new MOnGoldChange());
			this.gear4UpgradeState[worldState.gear4].isUpgraded = true;
			worldState.gear4 += 1;
			worldState.player.state.speed[3] -= 1;
			if (worldState.gear4 == this.gear4Costs.length) this.gear4UB.visible = false;
			if (worldState.gear4 < gear4Costs.length) gear4UB.amount = gear4Costs[worldState.gear4];
		});
		this.gear4UB.setX(83).setY(107);

		this.display.addChild(this.gear5UB = new ClipboardUpgradeButton(gear5Costs[0]));
		this.gear5UB.tooltipWindow = new TooltipWindow(S.get("upgrades.gear.tooltip"));
		this.gear5UB.tooltipShowConf = {preferredDirection: [Right, Up, Left, Down]};
		this.gear5UB.tooltipHelper = system.tooltipHelper;
		this.gear5UB.getTooltipBounds = () -> {
			return this.gear5UB.getBounds(system.drawLayers);
		}
		this.gear5UB.addOnClickListener("Clipboard", (e) -> {
			final worldState = this.system.world.worldState;
			if (worldState == null) return;
			if (worldState.gear5 >= this.gear5Costs.length) return;
			if (worldState.money < this.gear5Costs[worldState.gear5]) return;
			worldState.money -= this.gear5Costs[worldState.gear5];
			this.system.dispatcher.dispatch(new MOnGoldChange());
			this.gear5UpgradeState[worldState.gear5].isUpgraded = true;
			worldState.gear5 += 1;
			worldState.player.state.speed[4] -= 1;
			if (worldState.gear5 == this.gear5Costs.length) this.gear5UB.visible = false;
			if (worldState.gear5 < gear5Costs.length) gear5UB.amount = gear5Costs[worldState.gear5];
		});
		this.gear5UB.setX(83).setY(118);

		/**
			this.foodUpgradeState = [];
			for (i in 0...foodCosts.length) {
				final state = new UpgradeState(this);
				this.foodUpgradeState.push(state);
				state.setX(58 + (i * 6)).setY(32);
				this.display.addChild(state);
			}

			this.seatUpgradeState = [];
			for (i in 0...seatCosts.length) {
				final state = new UpgradeState(this);
				this.seatUpgradeState.push(state);
				state.setX(58 + (i * 6)).setY(47);
				this.display.addChild(state);
			}
		**/

		this.fuelUpgradeState = [];
		for (i in 0...fuelCosts.length) {
			final state = new UpgradeState(this);
			this.fuelUpgradeState.push(state);
			state.setX(58 + (i * 6)).setY(61);
			this.display.addChild(state);
		}

		this.gear1UpgradeState = [];
		for (i in 0...gear1Costs.length) {
			final state = new UpgradeState(this);
			this.gear1UpgradeState.push(state);
			state.setX(58 + (i * 6)).setY(76);
			this.display.addChild(state);
		}

		this.gear2UpgradeState = [];
		for (i in 0...gear2Costs.length) {
			final state = new UpgradeState(this);
			this.gear2UpgradeState.push(state);
			state.setX(58 + (i * 6)).setY(87);
			this.display.addChild(state);
		}

		this.gear3UpgradeState = [];
		for (i in 0...gear3Costs.length) {
			final state = new UpgradeState(this);
			this.gear3UpgradeState.push(state);
			state.setX(58 + (i * 6)).setY(98);
			this.display.addChild(state);
		}

		this.gear4UpgradeState = [];
		for (i in 0...gear4Costs.length) {
			final state = new UpgradeState(this);
			this.gear4UpgradeState.push(state);
			state.setX(58 + (i * 6)).setY(109);
			this.display.addChild(state);
		}

		this.gear5UpgradeState = [];
		for (i in 0...gear5Costs.length) {
			final state = new UpgradeState(this);
			this.gear5UpgradeState.push(state);
			state.setX(58 + (i * 6)).setY(120);
			this.display.addChild(state);
		}

		this.closeButton = new ClipboardCloseButton();
		this.closeButton.x = 113;
		this.closeButton.y = 143;
		this.display.addChild(this.closeButton);
		this.closeButton.addOnClickListener("Clipboard", (e) -> {
			this.close();
		});
	}

	function close() {
		this.system.closeMechanic();
	}

	public function init(world: World) {
		final dispatcher = world.dispatcher;

		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			reset();
		}, 0);
	}

	override function reset() {
		// for (u in this.foodUpgradeState) u.isUpgraded = false;
		// for (u in this.seatUpgradeState) u.isUpgraded = false;
		for (u in this.fuelUpgradeState) u.isUpgraded = false;
		for (u in this.gear1UpgradeState) u.isUpgraded = false;
		for (u in this.gear2UpgradeState) u.isUpgraded = false;
		for (u in this.gear3UpgradeState) u.isUpgraded = false;
		for (u in this.gear4UpgradeState) u.isUpgraded = false;
		for (u in this.gear5UpgradeState) u.isUpgraded = false;
		// this.foodUB.amount = this.foodCosts[0];
		// this.seatUB.amount = this.seatCosts[0];
		this.fuelUB.amount = this.fuelCosts[0];
		this.gear1UB.amount = this.gear1Costs[0];
		this.gear2UB.amount = this.gear2Costs[0];
		this.gear3UB.amount = this.gear3Costs[0];
		this.gear4UB.amount = this.gear4Costs[0];
		this.gear5UB.amount = this.gear5Costs[0];
		// this.foodUB.visible = true;
		// this.seatUB.visible = true;
		this.fuelUB.visible = true;
		this.gear1UB.visible = true;
		this.gear2UB.visible = true;
		this.gear3UB.visible = true;
		this.gear4UB.visible = true;
		this.gear5UB.visible = true;
	}
}
