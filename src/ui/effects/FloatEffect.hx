package ui.effects;

/**
	Refactor and move to zf later
	1. Remove these import
	2. > Effect.EffectConf
**/
import zf.effects.Effect;
import zf.effects.Effect.EffectConf;

typedef FloatEffectConf = {
	> EffectConf,

	/**
		How long it takes to cycle between min and max
	**/
	public var ?cycleDuration: Float;

	/**
		How much Y to move
	**/
	public var ?minY: Float;

	/**
		How little Y to move
	**/
	public var ?maxY: Float;
}

class FloatEffect extends Effect {
	public var conf: FloatEffectConf;

	var object: h2d.Object;

	var minY: Float = 0;
	var maxY: Float = 0;
	var dt: Float = 0;
	var cycleDuration: Float = 1;
	var halfDuration: Float;
	var currentApplied: Float; // the current y that is applied.

	public function new(object: h2d.Object, conf: FloatEffectConf) {
		super(conf);
		this.conf = conf;
		this.object = object;

		if (conf.cycleDuration != null) this.cycleDuration = conf.cycleDuration;
		this.halfDuration = this.conf.cycleDuration / 2;
		// we will first set the object scale to min straight away
		this.reset();
	}

	override public function update(dt: Float): Bool {
		return false;
	}
}
