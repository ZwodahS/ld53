package ui;

class PhoneMenuButton extends UIElement {
	public static final Size: Point2i = [87, 16];

	public var display: h2d.Object;

	public var frames: h2d.Anim;
	public var text: HtmlText;

	public function new(text: String) {
		super();

		this.addChild(this.display = new h2d.Object());
		this.display.addChild(this.frames = Assets.res.getAnim('phone:menu'));
		this.frames.pause = true;

		this.display.addChild(this.text = new HtmlText(Assets.getFont("display", 2)));
		this.text.text = text;
		this.text.textAlign = Center;
		this.text.textColor = 0xff111012;
		this.text.maxWidth = Size.x;
		this.text.y = 4;

		this.display.addChild(this.interactive = new Interactive(Size.x, Size.y));
	}

	override function updateRendering() {
		this.frames.currentFrame = 0;

		if (this.disabled == true) {
			this.frames.currentFrame = 0;
		} else if (this.isOver == true) {
			this.frames.currentFrame = 1;
		} else {
			this.frames.currentFrame = 0;
		}
	}
}

class Phone extends UIElement {
	public var display: h2d.Object;

	public var system: RenderSystem;

	public var targetPosition: Point2f = [0, 0];

	public function new(system: RenderSystem) {
		super();
		this.system = system;

		this.addChild(Assets.res.getBitmap("screen:phone"));
		this.addChild(this.display = new h2d.Object());
	}

	public function init(world: World) {
		final dispatcher = world.dispatcher;

		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			this.display.removeChildren(); // remove everything
		}, 0);

		dispatcher.listen(MOnGameover.MessageType, (message: zf.Message) -> {
			final m: MOnGameover = cast message;
			showGameover(m.gameOverType);
		}, 100);

		dispatcher.listen(MOnReturnHome.MessageType, (message: zf.Message) -> {
			showEndOfDay();
		}, 100);

		dispatcher.listen(MOnParcelChoosing.MessageType, (message: zf.Message) -> {
			final m: MOnParcelChoosing = cast message;
			showParcelChoosing(m);
		}, 5);

		dispatcher.listen(MOnParcelSelect.MessageType, (message: zf.Message) -> {
			final m: MOnParcelSelect = cast message;
			handleMOnParcelSelect(m);
		}, 100);

		dispatcher.listen(MOnDayBegin.MessageType, (message: zf.Message) -> {
			hideAll();
		}, 0);
	}

	public function update(dt: Float) {
		if (this.x != this.targetPosition.x) {
			if (this.x > this.targetPosition.x) {
				this.x -= Math.clampF(dt * 200, 0, (this.x - this.targetPosition.x));
			} else {
				this.x += Math.clampF(dt * 200, 0, (this.targetPosition.x - this.x));
			}
		}

		if (this.y != this.targetPosition.y) {
			if (this.y > this.targetPosition.y) {
				this.y -= Math.clampF(dt * 200, 0, (this.y - this.targetPosition.y));
			} else {
				this.y += Math.clampF(dt * 200, 0, (this.targetPosition.y - this.y));
			}
		}
	}

	public function showMenu() {
		final buttons: Array<PhoneMenuButton> = [];
		final button = new PhoneMenuButton("New Game");
		button.addOnClickListener("Phone", (e) -> {
			this.system.world.startGame();
		});
		buttons.push(button);

		final button = new PhoneMenuButton("Help");
		button.addOnClickListener("Phone", (e) -> {
			this.system.showHelp();
		});
		buttons.push(button);

		show(null, buttons);
	}

	public function showGameover(type: String) {
		final message = switch (type) {
			case "time": "Fail to report to warehouse on time.<br/><br />You are FIRED!";
			case "fuel": "You run out of Fuel...<br /><br />You are FIRED!";
			default: "Gameover";
		};

		final buttons: Array<PhoneMenuButton> = [];
		final button = new PhoneMenuButton("Restart");
		button.addOnClickListener("Phone", (e) -> {
			this.system.world.startGame();
		});
		buttons.push(button);

		final button = new PhoneMenuButton("Help");
		button.addOnClickListener("Phone", (e) -> {
			this.system.showHelp();
		});
		buttons.push(button);

		final score = 'Parcel Delivered: ${this.system.world.worldState.completedParcels}';

		show(message, buttons, score);
	}

	public function showEndOfDay() {
		final buttons: Array<PhoneMenuButton> = [];
		final button = new PhoneMenuButton("Claim");
		var earnAmount = this.system.world.worldState.parcelToday * 10;
		button.addOnClickListener("Phone", (e) -> {
			this.system.world.worldState.money += earnAmount;
			this.system.world.dispatcher.dispatch(new MOnGoldChange());
			this.system.world.startNextDay();
		});
		buttons.push(button);
		show('End of day<br/>Money earned: $$${earnAmount}', buttons);
	}

	var parcelText: HtmlText;

	public function showParcelChoosing(m: MOnParcelChoosing) {
		final buttons: Array<PhoneMenuButton> = [];
		final button = new PhoneMenuButton("Start Day");
		button.addOnClickListener("Phone", (e) -> {
			this.system.world.beginDelivery();
		});
		buttons.push(button);
		this.parcelText = show('Choose at least ${this.system.world.worldState.requiredParcel} Parcels.', buttons);
	}

	function handleMOnParcelSelect(m: MOnParcelSelect) {
		if (this.parcelText == null) return;
		var count = 0;
		for (parcel in this.system.world.worldState.parcels) {
			if (parcel.isAccepted == true) count += 1;
		}
		final required = this.system.world.worldState.requiredParcel;
		this.parcelText.text = 'Choose at least ${required} Parcels. <br />(${count}/${required})';
	}

	function show(message: String, buttons: Array<PhoneMenuButton>, postText: String = null) {
		hideAll();

		final flow = G.ui.make("<layout-vflow spacing='5' />");

		var text: HtmlText = null;
		if (message != null) {
			text = cast G.ui.make('<text fontName="display.1" textColor="white" maxWidth="100"/>');
			text.text = message;
			flow.addChild(text);
		}

		if (buttons != null) {
			for (button in buttons) flow.addChild(button);
		}

		if (postText != null) {
			final t: HtmlText = cast cast G.ui.make('<text fontName="display.1" textColor="white" maxWidth="100"/>');
			t.text = postText;
			flow.addChild(t);
		}

		flow.setX(12).setY(23);
		this.display.addChild(flow);
		return text;
	}

	function hideAll() {
		this.display.removeChildren();
		this.parcelText = null;
	}
}
