/**
	Assets is used to store loaded assets
**/
class Assets {
	public static var res: ResourceManager;

	public static var lang: String = "default";

	public static var gridFactory: Map<String, ScaleGridFactory>;

	public static function load() {
		Assets.res = new ResourceManager();
		Assets.res.load("config.json");

		Assets.gridFactory = new Map<String, ScaleGridFactory>();
		final gf = Assets.gridFactory;
		gf["box1"] = new ScaleGridFactory(Assets.res.getTile("ui:box1"), 5, 5);
	}

	public static function getFont(id: String, sizeIndex: Int): h2d.Font {
		return Assets.res.getFont(Assets.lang, id, sizeIndex);
	}

	public static function fromColor(color: Color, width: Float, height: Float): h2d.Bitmap {
		final bm = new h2d.Bitmap(Assets.res.getTile("white"));
		bm.width = width;
		bm.height = height;
		bm.color.setColor(color);
		return bm;
	}

	public static function displayStr(str: String, display: h2d.Object, color: Null<Color> = null, spacing: Int = 1) {
		for (i => s in str) {
			var bm = Assets.res.getBitmap('n:${String.fromCharCode(s)}');
			if (bm == null) bm = Assets.res.getBitmap('n: ');
			bm.x = i * (3 + spacing);
			if (color != null) bm.color.setColor(color);
			display.addChild(bm);
		}
		return display;
	}

	public static function loadImage(url: String): h2d.Tile {
		final t = Assets.res.getTile(url);
		return t;
	}
}
