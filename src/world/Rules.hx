package world;

import world.entities.factories.EntityFactory;

class Rules implements Identifiable {
	public function identifier(): String {
		return "Rules";
	}

	/**
		Store all the entity factories
	**/
	public var entities: Map<String, EntityFactory>;

	public var structLoader: zf.StructLoader;

	public var interp: hscript.Interp;
	public var parser: hscript.Parser;

	public function new() {
		this.entities = new Map<String, EntityFactory>();
		inline function registerFactory(factory: EntityFactory) {
			this.entities[factory.typeId] = factory;
		}
		registerFactory(new RoadFactory(this));
		registerFactory(new PlayerFactory(this));
		registerFactory(new BuildingFactory(this, {
			id: "building:mall",
			assetId: "mall",
			allowedQuest: [ParcelIn, FoodOut, PeopleIn, PeopleOut],
		}));
		registerFactory(new BuildingFactory(this, {
			id: "building:house",
			assetId: "house",
			allowedQuest: [ParcelIn, FoodIn, PeopleIn, PeopleOut],
		}));
		registerFactory(new BuildingFactory(this, {
			id: "building:warehouse",
			assetId: "warehouse",
		}));
		registerFactory(new BuildingFactory(this, {
			id: "building:petrolstation",
			assetId: "petrolstation",
		}));
		registerFactory(new BuildingFactory(this, {
			id: "building:mechanic",
			assetId: "mechanic",
		}));
		registerFactory(new FixtureFactory(this, {
			id: "fixture:tree",
			assetId: "tree",
		}));

		// ---- Set up struct loader ---- //
		this.structLoader = new zf.StructLoader();

		// ---- Set up hscript ---- //
		this.parser = new hscript.Parser();
		this.interp = new hscript.Interp();
	}

	// ---- Loader ---- //
	public function loadConfig(path: String) {
		final configPath = new haxe.io.Path(path);
		final expr = this.structLoader.loadFile(path);
		final ast = this.parser.parseString(expr);
		final defaultConf: RulesConf = this.interp.execute(ast);
	}

	// ---- HScript ---- //
	function exec(path: String): Dynamic {
		try {
			final expr = this.structLoader.loadFile(path);
			return executeScript(expr);
		} catch (e) {
			Logger.exception(e);
			Logger.warn('Fail to parse: ${path}');
			return null;
		}
	}

	inline public function executeScript(str: String): Dynamic {
		try {
			return this.interp.execute(this.parser.parseString(str));
		} catch (e) {
			return null;
		}
	}

	/**
		The actual layout
	**/
	// @formatter:off
	public static final LevelLayoutString = [
		" HHHHHHHHHHHHHHHH ",
		"HRRRRRRRRRRRRRRRRH",
		"HRHHHHHRHHHHHHHHRH",
		"HRH HHHRHHRRRRRRRH",
		"HRHHRRRRRRRHHRPHRH",
		"mRHHRHHHHHRHHRHHRH",
		"HRMMRMMMMMRMMMMMRH",
		"HRRRRRRRRRRRRRRRRH",
		" HHHRMMMMMMMMmMMRH",
		"HRRRRHH H H H HHRH",
		" HPHRHRHRHRHRHRHRH",
		" HHHRHRHRHRHRHRHRH",
		"WRRRRRRRRRRRRRRRRH",
		" HHHHHHHHHHHHHHHH ",
	];

	// ---- Make / Save / Load ---- //
	public function newGame(): WorldState {
		final state = new WorldState(this, Random.int(0, zf.Constants.SeedMax));

		// add the player as entity 0
		final player = this.entities["player"].make(state.nextId, state, {});
		state.entities.add(player);
		player.position.x = 1;
		player.position.y = 12;
		state.player = cast player;

		// create the static position
		for (y => str in LevelLayoutString) {
			for (x in 0...str.length) {
				final c = str.charAt(x);
				final entity = makeEntityFromChar(state, c);
				if (entity == null) continue;
				entity.position.x = x;
				entity.position.y = y;
				state.level.set(x, y, entity);
				state.entities.add(entity);
				// @hack
				if (c == "W") state.warehouse = cast entity;
			}
		}

		return state;
	}

	function makeEntityFromChar(state: WorldState, c: String): Entity {
		switch (c) {
			case "R":
				return this.entities["road"].make(state.nextId, state, {});
			case "M":
				return this.entities["building:mall"].make(state.nextId, state, {});
			case "H":
				return this.entities["building:house"].make(state.nextId, state, {});
			case "W":
				return this.entities["building:warehouse"].make(state.nextId, state, {});
			case "P":
				return this.entities["building:petrolstation"].make(state.nextId, state, {});
			case "m":
				return this.entities["building:mechanic"].make(state.nextId, state, {});
			case " ":
				return this.entities["fixture:tree"].make(state.nextId, state, {});
			default:
		}
		return null;
	}

	/**
		Load A WorldState from path
	**/
	public function loadFromPath(userdata: UserData, path: String): WorldState {
		// this is hard to do since we cannot read directory on web
		// we will need to construct the folder ourselves rather than recursively load it in WorldSaveFolder
		final fullPath = haxe.io.Path.join([path, "world.json"]);
		final result = userdata.loadFromPath(fullPath);
		var data: Dynamic = null;
		switch (result) {
			case SuccessContent(stringData):
				data = haxe.Json.parse(stringData);
			default:
				Logger.warn('Fail to load');
				return null;
		}
		return this.load(data);
	}

	/**
		Load a WorldState from a data struct
	**/
	public function load(data: Dynamic): WorldState {
		final context = new SerialiseContext();
		final option: SerialiseOption = {};
		final state = new WorldState(this, Random.int(0, zf.Constants.SeedMax));
		state.loadStruct(context, option, data);
		return state;
	}

	/**
		Save a world state to path
	**/
	public function saveToPath(userdata: UserData, worldState: WorldState, path: String) {
		final context = new SerialiseContext();
		final worldStateSF = worldState.toStruct(context, {});
		final fullpath = haxe.io.Path.join([path, "world.json"]);
#if sys
		final jsonString = haxe.format.JsonPrinter.print(worldStateSF, "  ");
#else
		final jsonString = haxe.Json.stringify(worldStateSF);
#end
		final result = userdata.saveToPath(fullpath, jsonString);
		switch (result) {
			case Success:
			default:
				Logger.warn('Fail to save');
		}
	}
}
