package world.entities;

class Player extends Entity {
	public var state: PlayerStateComponent;

	public function new(id: Int, factory: PlayerFactory) {
		super(id, factory);
		this.kind = EntityKind.Player;
	}
}
