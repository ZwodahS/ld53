package world.entities;

class Fixture extends Entity {
	public function new(id: Int, factory: FixtureFactory) {
		super(id, factory);
		this.kind = EntityKind.Fixture;
	}
}
