package world.entities.components;

typedef PositionComponentSF = {
	public var x: Int;
	public var y: Int;
}

class PositionComponent extends Component {
	public static final ComponentType = "PositionComponent";

	override public function get_typeId(): String {
		return ComponentType;
	}

	public var x: Int;
	public var y: Int;

	public function getPoint(pt: Point2i = null): Point2i {
		if (pt == null) return new Point2i(this.x, this.y);
		pt.x = this.x;
		pt.y = this.y;
		return pt;
	}

	public function new() {}

	override public function toStruct(context: SerialiseContext, option: SerialiseOption): Dynamic {
		return {
			x: this.x,
			y: this.y,
		};
	}

	override public function loadStruct(context: SerialiseContext, option: SerialiseOption, conf: Dynamic): Component {
		final sf: PositionComponentSF = cast conf;
		sf.x = this.x;
		sf.y = this.y;
		return this;
	}
}
