package world.entities.components;

class FixtureRenderComponent extends RenderComponent {
	override public function onWorldSet(world: World) {
		final assetId = cast(this.entity.factory, FixtureFactory).conf.assetId;
		this.map.addChild(Assets.res.getBitmap('tiles:${assetId}'));
	}
}
