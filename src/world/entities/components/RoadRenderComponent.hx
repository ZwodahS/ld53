package world.entities.components;

import world.entities.Road.Exit;

class RoadRenderComponent extends RenderComponent {
	public function new() {
		super();
	}

	override public function onWorldSet(world: World) {
		final state = world.worldState;
		final position = this.entity.position;
		var bitmapIndex = 0;

		inline function isRoad(positionX: Int, positionY: Int): Bool {
			final e = state.level.get(positionX, positionY);
			if (e == null) return false;
			if (e.typeId != "road") return false;
			return true;
		}

		if (isRoad(position.x, position.y - 1)) bitmapIndex += Exit.North;
		if (isRoad(position.x, position.y + 1)) bitmapIndex += Exit.South;
		if (isRoad(position.x - 1, position.y)) bitmapIndex += Exit.West;
		if (isRoad(position.x + 1, position.y)) bitmapIndex += Exit.East;

		this.map.addChild(Assets.res.getBitmap("tiles:road", bitmapIndex));
	}
}
