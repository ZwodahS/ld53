package world.entities.components;

/**
	The main render component for all entities

	This does not provide anything by default.
**/
class RenderComponent extends Component {
	public var map: h2d.Object;

	public function new() {
		this.map = new h2d.Object();
	}

	public function onWorldSet(world: World) {}

	override public function dispose() {}

	public function sync() {}
}
