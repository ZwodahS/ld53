package world.entities.components;

import ui.QuestButton;

class BuildingRenderComponent extends RenderComponent {
	override public function onWorldSet(world: World) {
		final assetId = cast(this.entity.factory, BuildingFactory).conf.assetId;
		this.map.addChild(Assets.res.getBitmap('tiles:${assetId}'));
		final interactive = UIElement.makeWithInteractive([16, 16]);
		this.map.addChild(interactive);
		interactive.tooltipWindow = new ui.TooltipWindow(S.get('buildings.${entity.typeId}.tooltip'));
		interactive.tooltipShowConf = {preferredDirection: [Up, Down, Right, Left]};
		interactive.tooltipHelper = world.renderSystem.tooltipHelper;
		interactive.getTooltipBounds = () -> {
			return interactive.getBounds(world.renderSystem.drawLayers);
		};
		this.map.addChild(this.buttonLayer = new h2d.Object());
		this.buttonLayer.y = -3;
	}

	/**
		overrides the button from quest
	**/
	public var overrideButton(default, set): QuestButton;

	var buttonLayer: h2d.Object;

	public function set_overrideButton(v: QuestButton): QuestButton {
		this.overrideButton = v;
		updateRenderedButton();
		return this.overrideButton;
	}

	function updateRenderedButton() {
		this.buttonLayer.removeChildren();
		if (this.overrideButton != null) {
			this.buttonLayer.addChild(overrideButton);
		}
	}
}
