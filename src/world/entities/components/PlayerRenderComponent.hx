package world.entities.components;

class PlayerRenderComponent extends RenderComponent {
	public var up: h2d.Anim;
	public var down: h2d.Anim;
	public var left: h2d.Anim;
	public var right: h2d.Anim;

	override public function onWorldSet(world: World) {
		this.map.addChild(this.up = Assets.res.getAnim("tiles:player:up"));
		this.map.addChild(this.down = Assets.res.getAnim("tiles:player:down"));
		this.map.addChild(this.left = Assets.res.getAnim("tiles:player:left"));
		this.map.addChild(this.right = Assets.res.getAnim("tiles:player:right"));

		this.up.speed = 5;
		this.down.speed = 5;
		this.left.speed = 5;
		this.right.speed = 5;
		this.up.visible = false;
		this.down.visible = false;
		this.left.visible = false;
		this.right.visible = false;

		sync();
	}

	override public function sync() {
		final player: Player = cast this.entity;
		this.up.visible = false;
		this.down.visible = false;
		this.left.visible = false;
		this.right.visible = false;
		switch (player.state.currentDirection) {
			case East:
				this.right.visible = true;
			case West:
				this.left.visible = true;
			case North:
				this.up.visible = true;
			case South:
				this.down.visible = true;
			default:
		}
	}
}
