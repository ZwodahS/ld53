package world.entities.components;

typedef PlayerStateComponentSF = {
	public var fuel: Int;
	public var maxFuel: Int;
	public var gear: Int;
	public var speed: Array<Int>;
	public var money: Int;
}

class PlayerStateComponent extends Component {
	public static final ComponentType = "PlayerStateComponent";

	override public function get_typeId(): String {
		return ComponentType;
	}

	public var fuel: Int = 0;
	public var maxFuel: Int = 0;

	// the current Gear - 0 index
	public var gear: Int = 0;

	/**
		speed store the time it takes to move at that gear
	**/
	public var speed: Array<Int>;

	public var currentDirection: Direction = East;

	public function new() {
		this.fuel = 80;
		this.maxFuel = 80;
		this.gear = 0;
		this.speed = [12, 10, 8, 6, 4];
	}

	override public function toStruct(context: SerialiseContext, option: SerialiseOption): Dynamic {
		return {
			fuel: this.fuel,
			maxFuel: this.maxFuel,
			gear: this.gear,
			speed: this.speed,
		};
	}

	override public function loadStruct(context: SerialiseContext, option: SerialiseOption, conf: Dynamic): Component {
		final sf: PlayerStateComponentSF = cast conf;
		this.fuel = sf.fuel;
		this.maxFuel = sf.maxFuel;
		this.gear = sf.gear;
		this.speed = sf.speed;
		return this;
	}
}
