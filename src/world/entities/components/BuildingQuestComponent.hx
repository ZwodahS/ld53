package world.entities.components;

typedef BuildingQuestComponentSF = {}

class BuildingQuestComponent extends Component {
	public static final ComponentType = "BuildingQuestComponent";

	override public function get_typeId(): String {
		return ComponentType;
	}

	public var questTypes: Array<QuestType>;

	public var parcel(default, set): Parcel;

	public function set_parcel(v: Parcel): Parcel {
		this.parcel = v;
		return this.parcel;
	}

	public function new(questTypes: Array<QuestType>) {
		this.questTypes = questTypes;
	}

	override public function toStruct(context: SerialiseContext, option: SerialiseOption): Dynamic {
		return {};
	}

	override public function loadStruct(context: SerialiseContext, option: SerialiseOption, conf: Dynamic): Component {
		final sf: BuildingQuestComponentSF = cast conf;
		return this;
	}
}
