package world.entities;

class Building extends Entity {
	public var quest(default, set): BuildingQuestComponent;

	public function set_quest(component: BuildingQuestComponent): BuildingQuestComponent {
		final prev = this.quest;
		this.quest = component;
		onComponentChanged(prev, this.quest);
		return this.quest;
	}

	public function new(id: Int, factory: BuildingFactory) {
		super(id, factory);
		this.kind = EntityKind.Building;
	}
}
