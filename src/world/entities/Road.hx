package world.entities;

enum abstract Exit(Int) from Int to Int {
	public var North = 1;
	public var East = 2;
	public var South = 4;
	public var West = 8;
}

class Road extends Entity {
	public function new(id: Int, factory: RoadFactory) {
		super(id, factory);
		this.kind = EntityKind.Road;
	}
}
