package world.entities.factories;

typedef BuildingConf = {
	public var id: String;
	public var assetId: String;
	public var ?allowedQuest: Array<QuestType>;
}

class BuildingFactory extends EntityFactory {
	public var conf: BuildingConf;

	public function new(rules: Rules, conf: BuildingConf) {
		super(conf.id, rules);
		this.conf = conf;
		if (this.conf.allowedQuest == null) this.conf.allowedQuest = [];
	}

	// ---- Make / Save / Load ---- //
	override public function make(id: Int, worldState: WorldState, conf: Dynamic = null): Entity {
		final entity = new Building(id, this);

		entity.render = new BuildingRenderComponent();
		entity.position = new PositionComponent();
		entity.quest = new BuildingQuestComponent(this.conf.allowedQuest);

		return entity;
	}
}
