package world.entities.factories;

class PlayerFactory extends EntityFactory {
	public function new(rules: Rules) {
		super("player", rules);
	}

	// ---- Make / Save / Load ---- //
	override public function make(id: Int, worldState: WorldState, conf: Dynamic = null): Entity {
		final entity = new Player(id, this);

		entity.render = new PlayerRenderComponent();
		entity.position = new PositionComponent();
		entity.state = new PlayerStateComponent();

		return entity;
	}
}
