package world.entities.factories;

typedef FixtureConf = {
	public var id: String;
	public var assetId: String;
}

class FixtureFactory extends EntityFactory {
	public var conf: FixtureConf;

	public function new(rules: Rules, conf: FixtureConf) {
		super(conf.id, rules);
		this.conf = conf;
	}

	// ---- Make / Save / Load ---- //
	override public function make(id: Int, worldState: WorldState, conf: Dynamic = null): Entity {
		final entity = new Fixture(id, this);

		entity.render = new FixtureRenderComponent();
		entity.position = new PositionComponent();

		return entity;
	}
}
