package world.entities.factories;

class RoadFactory extends EntityFactory {
	public function new(rules: Rules) {
		super("road", rules);
	}

	// ---- Make / Save / Load ---- //
	override public function make(id: Int, worldState: WorldState, conf: Dynamic = null): Entity {
		final entity = new Road(id, this);

		entity.render = new RoadRenderComponent();
		entity.position = new PositionComponent();

		return entity;
	}
}
