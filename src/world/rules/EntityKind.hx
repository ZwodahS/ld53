package world.rules;

enum abstract EntityKind(String) from String to String {
	// Unknown, the entity is not inited.
	public var Unknown = "EK_Unknown";
	public var Road = "EK_Road";
	public var Building = "EK_Building";
	public var Player = "EK_Player";
	public var Fixture = "EK_Fixture";
}
