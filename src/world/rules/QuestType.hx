package world.rules;

enum abstract QuestType(String) from String to String {
	public var ParcelIn = "ParcelIn";
	public var FoodIn = "FoodIn";
	public var FoodOut = "FoodOut";
	public var PeopleIn = "PeopleIn";
	public var PeopleOut = "PeopleOut";
}
