package world.rules;

class Parcel {
	public var target: Building;
	public var isAccepted: Bool = false;

	public function new(target: Building) {
		this.target = target;
	}
}
