package world;

class World extends zf.engine2.World {
	/**
		Store the main rule object
	**/
	public var rules: Rules;

	// ---- Systems ---- //

	/**
		Main rendering system
	**/
	public var renderSystem: RenderSystem;

	/**
		Handle player input
	**/
	public var inputSystem: InputSystem;

	public var questSystem: QuestSystem;

	/**
		World State
	**/
	public var worldState(default, set): WorldState = null;

	public function set_worldState(s: WorldState): WorldState {
		this.worldState = s;
		this.dispatcher.dispatch(new MOnWorldStateSet());
		for (e in this.worldState.entities) {
			this.registerEntity(e);
		}
		this.state.set(GameState.Idle);
		return this.worldState;
	}

	public var nextId(get, never): Int;

	inline function get_nextId(): Int {
		return this.worldState == null ? 0 : this.worldState.nextId;
	}

	/**
		User Profile
	**/
	public var profile: Profile;

	/**
		World simulation speed.
	**/
	public var worldSpeed: Float = 1;

	public var state: zf.sm.StateManager;

	public function new(rules: Rules, profile: Profile) {
		super();
		this.rules = rules;
		this.profile = profile;

		this.addSystem(this.renderSystem = new RenderSystem());
		this.addSystem(this.inputSystem = new InputSystem());
		this.addSystem(new PlayerSystem());
		this.addSystem(new GameStateSystem());
		this.addSystem(new BuildingSystem());
		this.addSystem(this.questSystem = new QuestSystem());
		this.state = new zf.sm.StateManager();
		GameState.registerStates(this.state, this);
		this.state.onStateChanged = (p, n) -> {
			this.dispatcher.dispatch(new MOnGameStateChanged(p, n));
		}
		this.state.set(GameState.Menu);
	}

	public function startGame() {
		final worldState = this.rules.newGame();
		this.worldState = worldState;
		this.startNextDay();
	}

	public function startNextDay() {
		this.worldState.timeOfDay = 0;
		this.dispatcher.dispatch(new MOnTimeChange());
		this.worldState.parcelToday = 0;
		this.worldState.requiredParcel += 2;
		this.worldState.requiredParcel = Math.clampI(this.worldState.requiredParcel, 0, 20);
		final parcels = this.questSystem.generateParcel(this.worldState.requiredParcel + 5);
		this.worldState.parcels = parcels;
		this.dispatcher.dispatch(new MOnParcelChoosing(parcels));
	}

	public function beginDelivery() {
		var count = 0;
		for (parcel in this.worldState.parcels) {
			if (parcel.isAccepted == true) count += 1;
		}
		final required = this.worldState.requiredParcel;
		if (count < required) return;
		this.worldState.parcels.filterAndRemove((p) -> {
			return p.isAccepted == false;
		});
		this.dispatcher.dispatch(new MOnDayBegin());
	}

	public function save() {
		final fullpath = this.profile.profile.path("world");
		this.rules.saveToPath(Globals.savefile.userdata, this.worldState, fullpath);
	}

	public function load() {
		final fullPath = this.profile.profile.path("world");
		final worldState = this.rules.loadFromPath(Globals.savefile.userdata, fullPath);
		this.worldState = worldState;
	}
}
