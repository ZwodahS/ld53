package world.messages;

class MOnPlayerMoveFinish extends zf.Message {
	public static final MessageType = "MOnPlayerMoveFinish";

	public function new() {
		super(MessageType);
	}
}
