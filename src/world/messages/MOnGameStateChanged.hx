package world.messages;

import zf.sm.State;

class MOnGameStateChanged extends zf.Message {
	public static final MessageType = "MOnGameStateChanged";

	public var previous: State;
	public var next: State;

	public function new(p: State, n: State) {
		super(MessageType);
		this.previous = p;
		this.next = n;
	}

	override public function toString(): String {
		return '[m:MOnGameStateChanged: ${previous.name} -> ${next.name}]';
	}
}
