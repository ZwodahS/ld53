package world.messages;

class MOnFuelChange extends zf.Message {
	public static final MessageType = "MOnFuelChange";

	public function new() {
		super(MessageType);
	}
}
