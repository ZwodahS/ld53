package world.messages;

class MOnParcelDeliver extends zf.Message {
	public static final MessageType = "MOnParcelDeliver";

	public var parcel: Parcel;
	public var building: Building;

	public function new(parcel: Parcel, building: Building) {
		super(MessageType);
		this.parcel = parcel;
		this.building = building;
	}
	/**
		# Dispatchers
		# Listeners
	**/
}
