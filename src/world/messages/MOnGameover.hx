package world.messages;

class MOnGameover extends zf.Message {
	public static final MessageType = "MOnGameover";

	public var gameOverType: String;

	public function new(type: String) {
		super(MessageType);
		this.gameOverType = type;
	}
}
