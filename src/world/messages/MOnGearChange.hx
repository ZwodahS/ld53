package world.messages;

class MOnGearChange extends zf.Message {
	public static final MessageType = "MOnGearChange";

	public function new() {
		super(MessageType);
	}
}
