package world.messages;

class MOnPlayerMove extends zf.Message {
	public static final MessageType = "MOnPlayerMove";

	public var source: Point2i;
	public var destination: Point2i;
	public var direction: Direction;

	public function new(direction: Direction, source: Point2i, destination: Point2i) {
		super(MessageType);
		this.direction = direction;
		this.source = source;
		this.destination = destination;
	}
}
