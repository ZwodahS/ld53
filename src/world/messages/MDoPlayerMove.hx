package world.messages;

/**
	Handle player movements
**/
class MDoPlayerMove extends zf.Message {
	public static final MessageType = "MDoPlayerMove";

	public var direction: Direction;

	public function new(direction: Direction) {
		super(MessageType);
		this.direction = direction;
	}
}
