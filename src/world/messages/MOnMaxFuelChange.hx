package world.messages;

class MOnMaxFuelChange extends zf.Message {
	public static final MessageType = "MOnMaxFuelChange";

	public function new() {
		super(MessageType);
	}
}
