package world.messages;

class MOnParcelChoosing extends zf.Message {
	public static final MessageType = "MOnParcelChoosing";

	public var parcels: Array<Parcel>;

	public function new(parcels: Array<Parcel>) {
		super(MessageType);
		this.parcels = parcels;
	}
	/**
		# Dispatchers
		# Listeners
	**/
}
