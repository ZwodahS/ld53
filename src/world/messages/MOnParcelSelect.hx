package world.messages;

class MOnParcelSelect extends zf.Message {
	public static final MessageType = "MOnParcelSelect";

	public var parcel: Parcel;

	public function new(parcel: Parcel) {
		super(MessageType);
		this.parcel = parcel;
	}
	/**
		# Dispatchers
		# Listeners
	**/
}
