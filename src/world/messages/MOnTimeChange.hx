package world.messages;

class MOnTimeChange extends zf.Message {
	public static final MessageType = "MOnTimeChange";

	public function new() {
		super(MessageType);
	}
}
