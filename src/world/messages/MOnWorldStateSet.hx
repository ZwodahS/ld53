package world.messages;

class MOnWorldStateSet extends zf.Message {
	public static final MessageType = "MOnWorldStateSet";

	public function new() {
		super(MessageType);
	}
}
