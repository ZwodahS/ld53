package world.messages;

class MDoDeliver extends zf.Message {
	public static final MessageType = "MDoDeliver";

	public var building: Building;

	public function new(building: Building) {
		super(MessageType);
		this.building = building;
	}
	/**
		# Dispatchers
		# Listeners
	**/
}
