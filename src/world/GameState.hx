package world;

class GameState extends zf.sm.State {
	// Generic Wait State
	public static final Wait = "Wait";

	public static final Menu = "Menu";
	public static final Idle = "Idle";
	public static final PlayerMoving = "PlayerMoving";
	public static final EndOfDay = "EndOfDay";
	public static final ParcelChoosing = "ParcelChoosing";
	public static final Delivering = "Delivering";
	public static final PetrolStation = "PetrolStation";
	public static final Mechanic = "Mechanic";

	public var world(default, null): World;

	public function new(world: World, name: String) {
		super(name);

		this.world = world;
	}

	override public function copy(): GameState {
		final state = new GameState(this.world, this.name);
		return state;
	}

	public static function registerStates(stateManager: zf.sm.StateManager, world: World) {
		stateManager.registerState(new GameState(world, GameState.Menu));
		stateManager.registerState(new GameState(world, GameState.Idle));
		stateManager.registerState(new GameState(world, GameState.PlayerMoving));
		stateManager.registerState(new GameState(world, GameState.EndOfDay));
		stateManager.registerState(new GameState(world, GameState.ParcelChoosing));
		stateManager.registerState(new GameState(world, GameState.PetrolStation));
		stateManager.registerState(new GameState(world, GameState.Mechanic));
		stateManager.registerState(new zf.sm.Wait(GameState.Wait));
	}
}
