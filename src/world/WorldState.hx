package world;

typedef WorldStateSF = {
	public var ?intCounter: Int;
	public var ?entities: Array<EntitySF>;
}

class WorldState implements StructSerialisable implements Identifiable {
	public var r: hxd.Rand;

	// ---- Id generation ---- //

	/**
		Int counter for id generation
	**/
	var intCounter: zf.IntCounter.SimpleIntCounter;

	public var nextId(get, never): Int;

	public function get_nextId(): Int {
		return this.intCounter.getNextInt();
	}

	/**
		identifier
	**/
	public function identifier() {
		return "WorldState";
	}

	var rules: Rules;

	public var entities: Entities<Entity>;

	public var level: Vector2D<Entity>;

	public var player: Player;
	public var warehouse: Building;

	public var timeOfDay: Time;

	public var parcels: Array<Parcel>;
	public var parcelToday: Int = 0;
	public var requiredParcel: Int = 4;
	public var completedParcels: Int = 0;
	public var money: Int = 50;

	public var hasFoodDelivery: Bool = false;
	public var hasPeopleDelivery: Bool = false;
	public var fuelCapacity: Int = 0;
	public var gear1: Int = 0;
	public var gear2: Int = 0;
	public var gear3: Int = 0;
	public var gear4: Int = 0;
	public var gear5: Int = 0;

	public function new(rules: Rules, seed: Int = 0) {
		this.rules = rules;
		this.intCounter = new zf.IntCounter.SimpleIntCounter();
		this.r = new hxd.Rand(seed);
		this.entities = new Entities<Entity>();
		this.level = new Vector2D<Entity>(C.MapSize, null);
		this.timeOfDay = 0;
	}

	// ---- Save / Load ---- //
	public function toStruct(context: SerialiseContext, option: SerialiseOption): WorldStateSF {
		final entities: Entities<Entity> = new Entities<Entity>();
		context.add(entities);

		final stateSF: WorldStateSF = {};

		// store the id generators
		@:privateAccess stateSF.intCounter = this.intCounter.counter;

		// collect all the entities before this is called
		final entitiesSF = [for (entity in entities) entity.toStruct(context, option)];
		stateSF.entities = entitiesSF;

		return stateSF;
	}

	public function loadStruct(context: SerialiseContext, option: SerialiseOption, data: Dynamic): WorldState {
		final stateSF: WorldStateSF = cast data;
		final entitiesSF: Array<EntitySF> = stateSF.entities;
		final entities: Entities<Entity> = new Entities<Entity>();
		context.add(entities);

		for (sf in entitiesSF) {
			final factory = this.rules.entities[sf.typeId];
			if (factory == null) {
				Logger.warn('Fail to load entity, Type: ${sf.typeId}, Id: ${sf.id}');
				continue;
			}
			final entity = factory.load(context, option, sf);
			entities.add(entity);
		}

		// for each of the entities, now we can load the entity proper
		for (sf in entitiesSF) {
			final entity = entities.get(sf.id);
			if (entity == null) continue;
			entity.loadStruct(context, option, sf);
		}

		@:privateAccess this.intCounter.counter = stateSF.intCounter;
		return this;
	}
}
