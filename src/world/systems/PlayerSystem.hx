package world.systems;

class PlayerSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		dispatcher.listen(MDoPlayerMove.MessageType, (message: zf.Message) -> {
			final m: MDoPlayerMove = cast message;
			handleMDoPlayerMove(m);
		}, 0);

		dispatcher.listen(MOnPlayerMove.MessageType, (message: zf.Message) -> {
			final m: MOnPlayerMove = cast message;
			handleMOnPlayerMove(m);
		}, 100);

		dispatcher.listen(MOnParcelDeliver.MessageType, (message: zf.Message) -> {
			resetGear();
		}, 5);

		dispatcher.listen(MDoOpenMechanic.MessageType, (message: zf.Message) -> {
			resetGear();
		}, 5);

		dispatcher.listen(MDoOpenPetrol.MessageType, (message: zf.Message) -> {
			resetGear();
		}, 5);
	}

	function resetGear() {
		final player = this.world.worldState.player;
		player.state.gear = 0;
		this.dispatcher.dispatch(new MOnGearChange());
	}

	function handleMDoPlayerMove(m: MDoPlayerMove) {
		if (this.worldState == null || this.world.state.is(GameState.Idle) == false) return;

		final player = this.worldState.player;

		final source = new Point2i(player.position.x, player.position.y);
		final destination = new Point2i(player.position.x, player.position.y).move(m.direction);
		final road = this.worldState.level.get(destination.x, destination.y);
		if (road == null || road.kind != Road) return; // prevent move into building

		player.position.x = destination.x;
		player.position.y = destination.y;

		this.dispatcher.dispatch(new MOnPlayerMove(m.direction, source, destination));
	}

	function handleMOnPlayerMove(m: MOnPlayerMove) {
		final player = this.world.worldState.player;
		player.state.fuel = Math.clampI(player.state.fuel - 1, 0, null);
		this.dispatcher.dispatch(new MOnFuelChange());

		if (player.state.currentDirection == m.direction) {
			this.world.worldState.timeOfDay += player.state.speed[player.state.gear];
			this.dispatcher.dispatch(new MOnTimeChange());
			if (player.state.gear < 4) {
				player.state.gear += 1;
				this.dispatcher.dispatch(new MOnGearChange());
			}
		} else {
			if (player.state.currentDirection.opposite == m.direction) {
				player.state.gear = 0;
			} else {
				// @todo turn speed
				var turnCost = 3;
				player.state.gear = Math.clampI(player.state.gear - turnCost, 0, 4);
			}
			this.dispatcher.dispatch(new MOnGearChange());
			this.world.worldState.timeOfDay += player.state.speed[player.state.gear];
			this.dispatcher.dispatch(new MOnTimeChange());
		}
		player.state.currentDirection = m.direction;
		player.render.sync();
	}
}
