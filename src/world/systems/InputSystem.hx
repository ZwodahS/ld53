package world.systems;

class InputSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);
	}

	override public function onEvent(event: hxd.Event): Bool {
		return false;
	}

	/**
		reset the system to the same state after constructor
	**/
	override public function reset() {
		super.reset();
	}

	/**
		Dispose the system data
	**/
	override public function dispose() {
		super.dispose();
	}

	/**
		update loop
	**/
	override public function update(dt: Float) {
		super.update(dt);
		if (this.worldState == null || this.worldState.player == null) return;
		if (this.world.state.is(GameState.Idle) == false) return;

		var move: Direction = null;
		if (hxd.Key.isDown(hxd.Key.A) || hxd.Key.isDown(hxd.Key.LEFT)) move = move == null ? Left : None;
		if (hxd.Key.isDown(hxd.Key.D) || hxd.Key.isDown(hxd.Key.RIGHT)) move = move == null ? Right : None;
		if (hxd.Key.isDown(hxd.Key.W) || hxd.Key.isDown(hxd.Key.UP)) move = move == null ? Up : None;
		if (hxd.Key.isDown(hxd.Key.S) || hxd.Key.isDown(hxd.Key.DOWN)) move = move == null ? Down : None;

		if (move == null || move == None) return;

		this.world.dispatcher.dispatch(new MDoPlayerMove(move));
	}
}
