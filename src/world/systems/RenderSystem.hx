package world.systems;

import ui.*;

class RenderSystem extends System {
	// ---- All the various layers to draw into ---- //

	/**
		The main draw layer
	**/
	public final drawLayers: h2d.Layers;

	/**
		Background layers.
	**/
	public final bgLayers: h2d.Layers;

	/**
		Window layers, used by windowRenderSystem
	**/
	public final windowLayers: h2d.Layers;

	/**
		World rendering
	**/
	public final worldLayers: h2d.Layers;

	/**
		Tooltip layer
	**/
	public final tooltipLayers: h2d.Layers;

	/**
		Debug layers.
	**/
	public final worldDebugLayers: h2d.Layers;

	/**
		A non blocking animator.

		Use this for animation that is non-blocking
	**/
	public final animator: zf.up.Updater;

	/**
		WindowRenderSystem, a sub system for rendering windows
	**/
	public final windowRenderSystem: zf.ui.WindowRenderSystem;

	/**
		TooltipRenderSystem, a sub system for rendering tooltips
	**/
	public final tooltipRenderSystem: zf.ui.WindowRenderSystem;

	/**
		Tooltip Helper to attach tooltip to any h2d.Object.

		For game entity, it is better to use tooltip component.
	**/
	public final tooltipHelper: zf.ui.TooltipHelper;

	public final petrolDisplay: PetrolStation;
	public final levelDisplay: LevelDisplay;
	public final mechanicDisplay: Clipboard;

	public final fuelDisplay: FuelDisplay;

	public final gearDisplay: GearDisplay;

	public final moneyDisplay: BitmapDisplay;
	public final timeDisplay: BitmapDisplay;
	public final numParcelDisplay: BitmapDisplay;
	public final deliveredParcelDisplay: BitmapDisplay;

	public final phone: Phone;

	public static final PhoneActivePosition: Point2i = [425, 110];
	public static final PhoneMenuPosition: Point2i = [211, 30];

	public function new() {
		super();
		this.animator = new zf.up.Updater();

		// ---- Setup all the various layers ---- //
		this.drawLayers = new h2d.Layers();
		this.drawLayers.add(this.bgLayers = new h2d.Layers(), 0);
		this.drawLayers.add(this.worldLayers = new h2d.Layers(), 50);
		this.drawLayers.add(this.windowLayers = new h2d.Layers(), 100);
		this.drawLayers.add(this.tooltipLayers = new h2d.Layers(), 105);
		this.worldLayers.add(this.worldDebugLayers = new h2d.Layers(), 200);
		this.bgLayers.addChild(Assets.fromColor(0xffc4da88, Globals.game.gameWidth, Globals.game.gameHeight));

		// ---- Setup WindowRenderSystem ---- //
		final windowBounds = new h2d.col.Bounds();
		windowBounds.xMin = 15;
		windowBounds.yMin = 15;
		windowBounds.xMax = Globals.game.gameWidth - 15;
		windowBounds.yMax = Globals.game.gameHeight - 15;
		this.windowRenderSystem = new zf.ui.WindowRenderSystem(windowBounds, this.windowLayers);
		this.windowRenderSystem.defaultRenderDirection = [Down, Right, Left, Up];
		this.windowRenderSystem.defaultSpacing = 5;

		this.tooltipRenderSystem = new zf.ui.WindowRenderSystem(windowBounds, this.tooltipLayers);
		this.tooltipRenderSystem.defaultRenderDirection = [Down, Up, Right, Left];
		this.tooltipRenderSystem.defaultSpacing = 2;

		this.tooltipHelper = new zf.ui.TooltipHelper(this.tooltipRenderSystem);

		this.levelDisplay = new LevelDisplay(this);
		this.levelDisplay.x = 131;
		this.levelDisplay.y = 11;
		this.worldLayers.addChild(this.levelDisplay);
		this.worldLayers.addChild(Assets.res.getBitmap("screen:bg"));
		// this.worldLayers.x = -50;

		this.phone = new Phone(this);
		this.phone.setX(PhoneMenuPosition.x).setY(PhoneMenuPosition.y + 100);
		this.phone.targetPosition = PhoneMenuPosition;
		this.worldLayers.addChild(this.phone);
		this.phone.showMenu();

		this.fuelDisplay = new FuelDisplay(this);
		this.fuelDisplay.x = 274;
		this.fuelDisplay.y = 243;
		this.worldLayers.addChild(this.fuelDisplay);
		this.fuelDisplay.tooltipHelper = this.tooltipHelper;
		final fuelBounds = new h2d.col.Bounds();
		fuelBounds.x = 274;
		fuelBounds.y = 243;
		fuelBounds.width = 65;
		fuelBounds.height = 37;
		// @formatter:off
		this.fuelDisplay.getTooltipBounds = () -> { return fuelBounds; };
		this.fuelDisplay.tooltipShowConf = {preferredDirection: [Up, Right, Left, Down]};

		this.gearDisplay = new GearDisplay(this);
		this.gearDisplay.x = 204;
		this.gearDisplay.y = 243;
		this.worldLayers.addChild(this.gearDisplay);
		this.gearDisplay.tooltipHelper = this.tooltipHelper;
		final gearBounds = new h2d.col.Bounds();
		gearBounds.x = 204;
		gearBounds.y = 243;
		gearBounds.width = 65;
		gearBounds.height = 37;
		// @formatter:off
		this.gearDisplay.getTooltipBounds = () -> { return gearBounds; };
		this.gearDisplay.tooltipShowConf = {preferredDirection: [Up, Right, Left, Down]};

		this.moneyDisplay = new BitmapDisplay("0");
		this.moneyDisplay.alignment = AnchorRight;
		this.moneyDisplay.x = 190;
		this.moneyDisplay.y = 237;
		this.worldLayers.addChild(this.moneyDisplay);

		this.numParcelDisplay = new BitmapDisplay("0");
		this.numParcelDisplay.alignment = AnchorRight;
		this.numParcelDisplay.x = 190;
		this.numParcelDisplay.y = 248;
		this.worldLayers.addChild(this.numParcelDisplay);

		this.timeDisplay = new BitmapDisplay("00:00");
		this.timeDisplay.alignment = AnchorRight;
		this.timeDisplay.x = 190;
		this.timeDisplay.y = 259;
		this.worldLayers.addChild(this.timeDisplay);

		this.deliveredParcelDisplay = new BitmapDisplay("0");
		this.deliveredParcelDisplay.alignment = AnchorRight;
		this.deliveredParcelDisplay.x = 190;
		this.deliveredParcelDisplay.y = 270;
		this.worldLayers.addChild(this.deliveredParcelDisplay);

		this.petrolDisplay = new PetrolStation(this);
		this.worldLayers.addChild(this.petrolDisplay);

		this.mechanicDisplay = new Clipboard(this);
		this.worldLayers.addChild(this.mechanicDisplay);
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen RenderSystem MOnWorldStateSet 50
		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			onLoad();
		}, 0);

		dispatcher.listen(MOnTimeChange.MessageType, (message: zf.Message) -> {
			final m: MOnTimeChange = cast message;
			handleMOnTimeChange(m);
		}, 100);

		dispatcher.listen(MOnGameover.MessageType, (message: zf.Message) -> {
			final m: MOnGameover = cast message;
			handleMOnGameover(m);
		}, 100);

		dispatcher.listen(MOnReturnHome.MessageType, (message: zf.Message) -> {
			final m: MOnReturnHome = cast message;
			handleMOnReturnHome(m);
		}, 100);

		dispatcher.listen(MOnDayBegin.MessageType, (message: zf.Message) -> {
			updateParcel();
		}, 200);

		dispatcher.listen(MOnParcelDeliver.MessageType, (message: zf.Message) -> {
			updateParcel();
		}, 200);

		dispatcher.listen(MOnGoldChange.MessageType, (message: zf.Message) -> {
			final m: MOnGoldChange = cast message;
			handleMOnGoldChange(m);
		}, 0);

		dispatcher.listen(MDoOpenPetrol.MessageType, (message: zf.Message) -> {
			final m: MDoOpenPetrol = cast message;
			handleMDoOpenPetrol(m);
		}, 0);

		dispatcher.listen(MDoOpenMechanic.MessageType, (message: zf.Message) -> {
			final m: MDoOpenMechanic = cast message;
			handleMDoOpenMechanic(m);
		}, 0);

		this.levelDisplay.init(cast world);
		this.phone.init(cast world);
		this.fuelDisplay.init(cast world);
		this.gearDisplay.init(cast world);
		this.petrolDisplay.init(cast world);
		this.mechanicDisplay.init(cast world);
	}

	function onLoad() {
		this.phone.targetPosition = PhoneActivePosition;
		this.timeDisplay.value = '${this.world.worldState.timeOfDay.displayString}';
		this.moneyDisplay.value = '${this.world.worldState.money}';
	}

	override public function reset() {
		this.animator.clear();
		this.windowLayers.removeChildren();
		this.worldLayers.removeChildren();
	}

	override public function update(dt: Float) {
		super.update(dt);
		this.animator.update(dt);
		this.fuelDisplay.update(dt);
		this.gearDisplay.update(dt);
		this.phone.update(dt);
	}

	function handleMOnTimeChange(m: MOnTimeChange) {
		this.timeDisplay.value = '${this.world.worldState.timeOfDay.displayString}';
	}

	function handleMOnGameover(m: MOnGameover) {
		this.phone.targetPosition = PhoneMenuPosition;
	}

	function handleMOnReturnHome(m: MOnReturnHome) {
		this.phone.targetPosition = PhoneActivePosition;
		this.world.state.set(GameState.EndOfDay);
	}

	function handleMDoOpenPetrol(m: MDoOpenPetrol) {
		this.petrolDisplay.x = 10;
		this.petrolDisplay.y = 90;
		this.petrolDisplay.visible = true;
	}

	function handleMDoOpenMechanic(m: MDoOpenMechanic) {
		this.mechanicDisplay.x = 6;
		this.mechanicDisplay.y = 113;
		this.mechanicDisplay.visible = true;
	}

	public function closePetrol() {
		this.world.state.set(GameState.Idle);
		this.petrolDisplay.visible = false;
	}

	public function closeMechanic() {
		this.world.state.set(GameState.Idle);
		this.mechanicDisplay.visible = false;
	}

	function handleMOnGoldChange(m: MOnGoldChange) {
		this.moneyDisplay.value = '${this.world.worldState.money}';
	}

	function updateParcel() {
		this.numParcelDisplay.value = '${this.worldState.parcels.length}';
		this.deliveredParcelDisplay.value = '${this.worldState.completedParcels}';
	}

	public function showHelp() {
		final helpWindow = new HelpWindow();
		this.windowLayers.addChild(helpWindow);
	}
}
