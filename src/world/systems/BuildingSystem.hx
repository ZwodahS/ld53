package world.systems;

import ui.QuestButton;

class BuildingSystem extends System {
	var pt1: Point2i;
	var pt2: Point2i;

	public function new() {
		super();
		this.pt1 = new Point2i();
		this.pt2 = new Point2i();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		dispatcher.listen(MOnPlayerMoveFinish.MessageType, (message: zf.Message) -> {
			final m: MOnPlayerMoveFinish = cast message;
			handleMOnPlayerMoveFinish(m);
		}, 100);

		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			reset();
		}, 0);

		dispatcher.listen(MDoReturnHome.MessageType, (message: zf.Message) -> {
			final m: MDoReturnHome = cast message;
			handleMDoReturnHome(m);
		}, 0);

		dispatcher.listen(MOnParcelChoosing.MessageType, (message: zf.Message) -> {
			final m: MOnParcelChoosing = cast message;
			handleMOnParcelChoosing(m);
		}, 5);

		dispatcher.listen(MOnDayBegin.MessageType, (message: zf.Message) -> {
			final m: MOnDayBegin = cast message;
			handleMOnDayBegin(m);
		}, 5);

		dispatcher.listen(MOnParcelDeliver.MessageType, (message: zf.Message) -> {
			final m: MOnParcelDeliver = cast message;
			handleMOnParcelDeliver(m);
		}, 100);
	}

	override public function reset() {
		super.reset();
		this.overrideButtons.clear();
	}

	/**
		Store all the buildings with the buttons
	**/
	var overrideButtons: Array<Building> = [];

	function clearOverrideButtons() {
		// hide all the existing hud
		for (b in overrideButtons) {
			cast(b.render, BuildingRenderComponent).overrideButton = null;
		}
		this.overrideButtons.clear();
	}

	function handleMOnParcelDeliver(m: MOnParcelDeliver) {
		this.overrideButtons.remove(m.building);
		final rc: BuildingRenderComponent = cast m.building.render;
		rc.overrideButton = null;
	}

	function handleMOnPlayerMoveFinish(m: MOnPlayerMoveFinish) {
		updateButtonStates();
	}

	function updateButtonStates() {
		final player = this.world.worldState.player;
		final playerPos = player.position.getPoint(pt1);
		for (building in this.overrideButtons) {
			final rc: BuildingRenderComponent = cast building.render;
			final pos = building.position.getPoint(pt2);
			rc.overrideButton.disabled = playerPos.isAdjacent(pos) == false;
		}
	}

	function handleMDoReturnHome(m: MDoReturnHome) {
		// make sure that we are adjacent to the warehouse
		final player = this.world.worldState.player;
		final warehouse = this.world.worldState.warehouse;

		final playerPos = player.position.getPoint(pt1);
		final warehousePos = warehouse.position.getPoint(pt2);
		if (playerPos.isAdjacent(warehousePos) == false) return;

		this.world.dispatcher.dispatch(new MOnReturnHome());
		clearOverrideButtons();
	}

	function handleMOnParcelChoosing(m: MOnParcelChoosing) {
		clearOverrideButtons();
		for (parcel in m.parcels) {
			final button = makeAcceptParcelButton(parcel);
			final rc: BuildingRenderComponent = cast parcel.target.render;
			rc.overrideButton = button;
			button.building = parcel.target;
			this.overrideButtons.push(parcel.target);
		}
	}

	function handleMOnDayBegin(m: MOnDayBegin) {
		clearOverrideButtons();
		for (parcel in this.world.worldState.parcels) {
			parcel.target.quest.parcel = parcel;
			final button = makeDeliveryButton(parcel);
			final rc: BuildingRenderComponent = cast parcel.target.render;
			rc.overrideButton = button;
			button.building = parcel.target;
			this.overrideButtons.push(parcel.target);
		}

		// handle warehouse specially

		// find all the different type of key building and add the button
		for (_ => building in this.world.worldState.level.iterateYX()) {
			final button = switch (building.typeId) {
				case "building:warehouse": makeReturnHomeButton();
				case "building:petrolstation": makePetrolButton();
				case "building:mechanic": makeMechanicButton();
				default: null;
			};
			if (button != null) {
				final rc: BuildingRenderComponent = cast building.render;
				rc.overrideButton = button;
				this.overrideButtons.push(cast building);
			}
		}
		updateButtonStates();
	}

	// ---- Button Factory ---- //

	function makeReturnHomeButton() {
		final button = new QuestButton('green', 'home');
		button.hideOnDisable = true;
		final window = new ui.TooltipWindow("");
		window.getTooltip = () -> {
			// @todo
			if (this.world.worldState.parcels.length > 0) return "Deliver all parcels before reporting back.";
			if (button.disabled == true) return "Must be adjacent to report.";
			return "Report";
		};
		button.tooltipWindow = window;
		button.tooltipHelper = this.world.renderSystem.tooltipHelper;
		button.tooltipShowConf = {preferredDirection: [Up, Down, Right, Left]}
		button.getTooltipBounds = () -> {
			button.getBounds(this.world.renderSystem.drawLayers);
		};
		button.addOnClickListener("BuildingSystem", (e) -> {
			if (this.world.worldState.parcels.length > 0) return;
			this.world.dispatcher.dispatch(new MDoReturnHome());
		});
		button.disabled = true;
		return button;
	}

	function makePetrolButton() {
		final button = new QuestButton('blue', 'petrol');
		button.hideOnDisable = true;
		final window = new ui.TooltipWindow("Refill your refuel");
		button.tooltipWindow = window;
		button.tooltipHelper = this.world.renderSystem.tooltipHelper;
		button.tooltipShowConf = {preferredDirection: [Up, Down, Right, Left]}
		button.getTooltipBounds = () -> {
			button.getBounds(this.world.renderSystem.drawLayers);
		};
		button.addOnClickListener("BuildingSystem", (e) -> {
			this.world.dispatcher.dispatch(new MDoOpenPetrol());
		});
		return button;
	}

	function makeMechanicButton() {
		final button = new QuestButton('blue', 'mechanic');
		button.hideOnDisable = true;
		final window = new ui.TooltipWindow("Upgrade your vehicle");
		button.tooltipWindow = window;
		button.tooltipHelper = this.world.renderSystem.tooltipHelper;
		button.tooltipShowConf = {preferredDirection: [Up, Down, Right, Left]}
		button.getTooltipBounds = () -> {
			button.getBounds(this.world.renderSystem.drawLayers);
		};
		button.addOnClickListener("BuildingSystem", (e) -> {
			this.world.dispatcher.dispatch(new MDoOpenMechanic());
		});
		return button;
	}

	function makeAcceptParcelButton(parcel: Parcel) {
		final button = new QuestButton('yellow', 'parcel');
		final window = new ui.TooltipWindow("");
		window.getTooltip = () -> {
			return parcel.isAccepted ? "Cancel Parcel" : "Accept Parcel";
		};
		button.tooltipWindow = window;
		button.tooltipHelper = this.world.renderSystem.tooltipHelper;
		button.tooltipShowConf = {preferredDirection: [Up, Down, Right, Left]}
		button.getTooltipBounds = () -> {
			button.getBounds(this.world.renderSystem.drawLayers);
		};
		button.addOnClickListener("BuildingSystem", (e) -> {
			parcel.isAccepted = !parcel.isAccepted;
			this.world.dispatcher.dispatch(new MOnParcelSelect(parcel));
			button.color = parcel.isAccepted ? 'green' : 'yellow';
			window.onShow();
		});
		return button;
	}

	function makeDeliveryButton(parcel: Parcel) {
		final button = new QuestButton('green', 'parcel');
		final window = new ui.TooltipWindow("");
		window.getTooltip = () -> {
			return button.disabled == true ? "Must be adjacent to deliver" : "Deliver Parcel (10mins)";
		};
		button.tooltipWindow = window;
		button.tooltipHelper = this.world.renderSystem.tooltipHelper;
		button.tooltipShowConf = {preferredDirection: [Up, Down, Right, Left]}
		button.getTooltipBounds = () -> {
			button.getBounds(this.world.renderSystem.drawLayers);
		};
		button.addOnClickListener("BuildingSystem", (e) -> {
			if (this.world.state.is(GameState.Idle) == false) return;
			this.world.dispatcher.dispatch(new MDoDeliver(parcel.target));
		});
		return button;
	}
}
