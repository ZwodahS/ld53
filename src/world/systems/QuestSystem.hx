package world.systems;

class QuestSystem extends System {
	/**
		Cache all the position of different kind of buildings
	**/
	public var questPositions: Map<QuestType, Array<Building>>;

	public function new() {
		super();
		this.questPositions = [];
		this.questPositions[QuestType.ParcelIn] = [];
		this.questPositions[QuestType.FoodIn] = [];
		this.questPositions[QuestType.FoodOut] = [];
		this.questPositions[QuestType.PeopleIn] = [];
		this.questPositions[QuestType.PeopleOut] = [];
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			onLoad();
		}, 0);

		dispatcher.listen(MDoDeliver.MessageType, (message: zf.Message) -> {
			final m: MDoDeliver = cast message;
			handleMDoDeliver(m);
		}, 100);
	}

	/**
		reset the system to the same state after constructor
	**/
	override public function reset() {
		super.reset();
		for (buildings in this.questPositions) {
			buildings.clear();
		}
	}

	function onLoad() {
		if (this.worldState == null) return;
		reset();
		for (_ => tile in this.worldState.level.iterateYX()) {
			if (tile.kind != Building) continue;
			final building: Building = cast tile;
			for (qt in building.quest.questTypes) {
				this.questPositions[qt].push(building);
			}
		}
	}

	public function generateParcel(count: Int): Array<Parcel> {
		final buildings = getBuildingPositions(ParcelIn, count);
		final parcels: Array<Parcel> = [];
		for (building in buildings) {
			parcels.push(new Parcel(building));
		}
		return parcels;
	}

	public function getBuildingPositions(qt: QuestType, count: Int): Array<Building> {
		var buildings = [];
		for (building in this.questPositions[qt]) {
			if (building.quest.parcel == null) buildings.push(building);
		}
		trace(buildings.length);
		final out = buildings.randomItems(this.worldState.r, count);
		return out;
	}

	function handleMDoDeliver(m: MDoDeliver) {
		if (this.world.state.is(GameState.Idle) == false) return;
		this.world.state.set(GameState.Delivering);
		final building = m.building;
		if (building.quest.parcel != null) { // delivering parcel
			final parcel = building.quest.parcel;
			this.world.worldState.parcels.remove(parcel);
			this.world.worldState.parcelToday += 1;
			this.world.worldState.completedParcels += 1;
			this.world.worldState.timeOfDay += 10;
			this.dispatcher.dispatch(new MOnTimeChange());
			this.dispatcher.dispatch(new MOnParcelDeliver(parcel, building));
		}
		this.world.state.set(GameState.Idle);
	}
}
