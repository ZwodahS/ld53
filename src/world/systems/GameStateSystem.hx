package world.systems;

class GameStateSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		final dispatcher = world.dispatcher;

		dispatcher.listen(MOnTimeChange.MessageType, (message: zf.Message) -> {
			final m: MOnTimeChange = cast message;
			handleMOnTimeChange(m);
		}, 100);

		dispatcher.listen(MOnFuelChange.MessageType, (message: zf.Message) -> {
			final m: MOnFuelChange = cast message;
			handleMOnFuelChange(m);
		}, 100);

		dispatcher.listen(MOnGameover.MessageType, (message: zf.Message) -> {
			this.world.state.set(GameState.Menu);
		}, 110);

		dispatcher.listen(MOnParcelChoosing.MessageType, (message: zf.Message) -> {
			this.world.state.set(GameState.ParcelChoosing);
		}, 0);

		dispatcher.listen(MOnDayBegin.MessageType, (message: zf.Message) -> {
			this.world.state.set(GameState.Idle);
		}, 0);

		dispatcher.listen(MDoOpenPetrol.MessageType, (message: zf.Message) -> {
			this.world.state.set(GameState.PetrolStation);
		}, 0);

		dispatcher.listen(MDoOpenMechanic.MessageType, (message: zf.Message) -> {
			this.world.state.set(GameState.Mechanic);
		}, 0);
	}

	function handleMOnTimeChange(m: MOnTimeChange) {
		if ((this.world.worldState.timeOfDay: Int) >= 900) {
			// @todo check if adjacent to the warehouse
			this.dispatcher.dispatch(new MOnGameover("time"), EndOfQueue);
		}
	}

	function handleMOnFuelChange(m: MOnFuelChange) {
		if ((this.world.worldState.player.state.fuel == 0)) {
			// @todo check if adjacent to petrol and have money
			this.dispatcher.dispatch(new MOnGameover("fuel"), EndOfQueue);
		}
	}
}
