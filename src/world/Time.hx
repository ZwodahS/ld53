package world;

enum abstract Time(Int) from Int to Int {
	/**
		Store the time from 8am, such time = 0 ==> 08:00, and max value is 900, which is 11pm
	**/
	public var displayString(get, never): String;

	public function get_displayString(): String {
		return '${8 + Std.int(this / 60)}'.lpad("0", 2) + ':' + '${this % 60}'.lpad("0", 2);
	}
}
