package screens;

class GameScreen extends zf.Screen {
	public var world: World;

	public function new(w: World) {
		super();
		this.world = w;
		this.addChild(this.world.renderSystem.drawLayers);
		var count: Int = 0;
		this.world.dispatcher.onBeforeMessage = (m) -> {
			Logger.debug('${"".lpad(" ", count)}Start - [m:${m.type}]');
			count += 1;
		}
		this.world.dispatcher.onAfterMessage = (m) -> {
			count -= 1;
			Logger.debug('${"".lpad(" ", count)}End - ${m}');
		}
	}

	override public function update(dt: Float) {
		this.world.update(dt);
	}

	override public function onEvent(event: hxd.Event) {
		this.world.onEvent(event);
	}

	override public function destroy() {
		if (this.world != null) this.world.dispose();
	}
}
